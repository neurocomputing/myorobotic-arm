import socket
import time


bind_ip = '0.0.0.0'
bind_port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.connect((bind_ip, bind_port))
l = [0, 100]
i = 0

while True:
    i += 1
    i %= 2
    server.sendall(bytes(str(l[i]), encoding="utf-8"))
    time.sleep(1)

