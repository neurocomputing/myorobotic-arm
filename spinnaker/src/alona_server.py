#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import pdb
import rospy
from std_msgs.msg import String
import time
from roboy_middleware_msgs.msg import MotorCommand
from std_srvs.srv import Trigger
# rospy.init_node("test")
# p = rospy.Publisher("/test", String, queue_size=1)
# #msg = MotorCommand()
# for i in range(3):
#     p.publish("hello")
#     time.sleep(0.5)
# rospy.loginfo("published")

import spynnaker8 as sim
from pyNN.random import RandomDistribution
import pylab
import numpy
from pyNN.utility.plotting import Figure, Panel
from datetime import datetime
import numpy as np
import socket

from spynnaker.pyNN.external_devices_models import AbstractEthernetTranslator
from spynnaker.pyNN.external_devices_models. \
    abstract_multicast_controllable_device import \
    AbstractMulticastControllableDevice, SendType

class EthernetDevice(AbstractMulticastControllableDevice):
    @property
    def device_control_partition_id(self):
        return 55

    @property
    def device_control_key(self):
        return 63

    @property
    def device_control_uses_payload(self):
        return True

    @property
    def device_control_min_value(self):
        return -100

    @property
    def device_control_max_value(self):
        return 100

    @property
    def device_control_timesteps_between_sending(self):
        return 4

    @property
    def device_control_send_type(self):
        return SendType.SEND_TYPE_ACCUM

    @property
    def device_control_scaling_factor(self):
        return 1

    #def __init__(self):
    #    super().__init__()
    #

class EthernetPacketTranslator(AbstractEthernetTranslator):

    def __init__(self):
        #rospy.loginfo("ROS init")
        rospy.init_node('spinn_ctl')
        self.publisher = rospy.Publisher('/roboy/middleware/MotorCommand', MotorCommand, tcp_nodelay=True, queue_size=0)
        self.msg = MotorCommand()
        self.msg.id = 3
        self.msg.motors = [2,3]
        rospy.loginfo("init done")

    def translate_control_packet(self, multicast_packet):
        #key = multicast_packet.key
        payload = multicast_packet.payload
        if payload < 15000:
            self.msg.set_points = [50,0]
        else:
            self.msg.set_points = [0, 50]

        self.publisher.publish(self.msg)

        # return super().translate_control_packet(multicast_packet)



# is this an infinite simulation?
infinite_run = True

sim.setup(timestep=1, max_delay=15)
sim.set_number_of_neurons_per_core(sim.SpikeSourcePoisson, 50)
n_neurons = 500
n_exc = int(round(n_neurons * 0.8))
n_inh = int(round(n_neurons * 0.2))
weight_exc = 0.1
weight_inh = -5.0 * weight_exc
weight_input = 0.001

pop_input = sim.Population(100, sim.SpikeSourcePoisson(rate=0), label="Input")

pop_exc = sim.Population(n_exc, sim.IF_curr_exp, label="Excitatory",
                         additional_parameters={"spikes_per_second": 100})
pop_inh = sim.Population(n_inh, sim.IF_curr_exp, label="Inhibitory",
                         additional_parameters={"spikes_per_second": 100})
stim_exc = sim.Population(
    n_exc, sim.SpikeSourcePoisson(rate=1000.0), label="Stim_Exc")
stim_inh = sim.Population(
    n_inh, sim.SpikeSourcePoisson(rate=1000.0), label="Stim_Inh")

delays_exc = RandomDistribution(
    "normal_clipped", mu=1.5, sigma=0.75, low=1.0, high=14.4)
weights_exc = RandomDistribution(
    "normal_clipped", mu=weight_exc, sigma=0.1, low=0, high=numpy.inf)
conn_exc = sim.FixedProbabilityConnector(0.1)
synapse_exc = sim.StaticSynapse(weight=weights_exc, delay=delays_exc)
delays_inh = RandomDistribution(
    "normal_clipped", mu=0.75, sigma=0.375, low=1.0, high=14.4)
weights_inh = RandomDistribution(
    "normal_clipped", mu=weight_inh, sigma=0.1, low=-numpy.inf, high=0)
conn_inh = sim.FixedProbabilityConnector(0.1)
synapse_inh = sim.StaticSynapse(weight=weights_inh, delay=delays_inh)
sim.Projection(
    pop_exc, pop_exc, conn_exc, synapse_exc, receptor_type="excitatory")
sim.Projection(
    pop_exc, pop_inh, conn_exc, synapse_exc, receptor_type="excitatory")
sim.Projection(
    pop_inh, pop_inh, conn_inh, synapse_inh, receptor_type="inhibitory")
sim.Projection(
    pop_inh, pop_exc, conn_inh, synapse_inh, receptor_type="inhibitory")

conn_stim = sim.OneToOneConnector()
synapse_stim = sim.StaticSynapse(weight=weight_exc, delay=1.0)
sim.Projection(
    stim_exc, pop_exc, conn_stim, synapse_stim, receptor_type="excitatory")
sim.Projection(
    stim_inh, pop_inh, conn_stim, synapse_stim, receptor_type="excitatory")

delays_input = RandomDistribution(
    "normal_clipped", mu=1.5, sigma=0.75, low=1.0, high=14.4)
weights_input = RandomDistribution(
    "normal_clipped", mu=weight_input, sigma=0.01, low=0, high=numpy.inf)
sim.Projection(pop_input, pop_exc, sim.AllToAllConnector(), sim.StaticSynapse(
    weight=weights_input, delay=delays_input))

pop_exc.initialize(v=RandomDistribution("uniform", low=-65.0, high=-55.0))
pop_inh.initialize(v=RandomDistribution("uniform", low=-65.0, high=-55.0))

if not infinite_run:
    pop_exc.record("spikes")

# Controlling the rate of the SpikeSourcePoisson
sim.external_devices.add_poisson_live_rate_control(pop_input)
poisson_control = sim.external_devices.SpynnakerPoissonControlConnection(
    poisson_labels=[pop_input.label])

# Output population of the network -- this will send packets with
# payload via Ethernet which need to be decoded

# Ethernet devices
devices = [EthernetDevice()]
# The neuron model to be used in the Output Population
t = EthernetPacketTranslator()
voltage_model = sim.external_devices.ExternalDeviceLifControl(
    devices, create_edges=False, translator=t)
# The Output Population
output_pop = sim.external_devices.EthernetControlPopulation(
    len(devices),
    model=voltage_model)
# Connect the Output Population
sim.Projection(pop_input, output_pop,
               sim.AllToAllConnector(), sim.StaticSynapse(
                weight=weights_input, delay=delays_input),
               label="Input-Output projection")


# Dealing with sockets
bind_ip = '0.0.0.0'
bind_port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(1)  # max backlog of connections
client_sock, address = server.accept()


def start_callback(label, connection):
    # for rate in [50, 10, 20]:
    #     time.sleep(10.0)
    #     connection.set_rates(label, [(i, rate) for i in range(100)])
    last_error = ''
    first_recv = True
    while True:
        try:
            request = client_sock.recv(1024).decode('utf-8')
            if first_recv:
                print("Ignore the first message...")
                first_recv = False
                continue
            if "get_weights" in str(request):
                print("I would try to get weights now")
                continue
            elif "shutdown" in str(request):
                print("Shutting down...")
                sim.external_devices.request_stop()
                break
            else:
                rate = int(request)
                print("[{:20}] {:10}".format("SET RATE", rate))
                connection.set_rates(label, [(i, rate) for i in range(100)])
        except Exception as e:
            if str(e) != last_error:
                print("[{:10}{:10}]".format("ERROR",
                    datetime.now().strftime("%H:%M:%S")), e)
                last_error = str(e)
    print("Pretty sure I'm shutting down")


print(pop_input.label)
poisson_control.add_start_resume_callback(pop_input.label, start_callback)

if infinite_run:
    sim.external_devices.run_forever()
    print("Simulation ending...")
    sim.end()
else:
    sim.run(5000)
    end_time = sim.get_current_time()
    data = pop_exc.get_data("spikes")

    spin_data = pop_exc.spinnaker_get_data("spikes")

    np.savez_compressed('spikes', spin_data=spin_data)

    sim.end()

    Figure(
        # raster plot of the presynaptic neuron spike times
        Panel(data.segments[0].spiketrains,
              yticks=True, markersize=2.0, xlim=(0, end_time)),
        title="Balanced Random Network",
        annotations="Simulated with {}".format(sim.name())
    )
    pylab.savefig('x.png')
    pylab.show()
