import socket
import time
import rospy
from roboy_middleware_msgs.msg import MotorCommand
from std_srvs.srv import Trigger


# 'localhost' as ip address works on Windows too
bind_ip = 'localhost'
bind_port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.connect((bind_ip, bind_port))
l = [0, 100]
i = 0
get_weights_counter = 10
shutdown_counter = 60

def cb(data):
    rospy.loginfo("yay")
    return True

def cb2(data):
    rospy.loginfo("yay message")
    return True


rospy.init_node('stest')
s = rospy.Subscriber('/roboy/middleware/MotorCommand', MotorCommand, cb2)
service = rospy.Service("woo", Trigger, cb)

while True:
    # Reading bit of the client
    # should be a separate thread
    # spinn_reply = server.recv(1024).decode('utf-8')
    # print(spinn_reply)

    # Writing bit of the client
    i += 1
    i %= 2
    server.sendall(bytes(str(l[i]), encoding="utf-8"))
    time.sleep(1)
    get_weights_counter -= 1
    if not get_weights_counter:
        server.sendall(bytes(str("get_weights"), encoding="utf-8"))
        get_weights_counter = 10
    shutdown_counter -= 1
    if not shutdown_counter:
        server.sendall(bytes(str("shutdown"), encoding="utf-8"))
