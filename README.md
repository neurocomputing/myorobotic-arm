# Myo robotic setup for ROS

This package provides 

1) Python control system interface with the myo-arm robot and
2) A cerebellum-based brain-like controller for the myo-arm robot

The remainder of this page will document the access to 1). 
For further information on how to use/operate the brain-inspired controller, please refer
to [the documentation for the cerebellum model](https://gitlab.com/neurocomputing/myorobotic-arm/-/tree/master/myo/src/cerebellum).

## Installation

### Using our install script

```bash
# 1. Clone this repository and enter
git clone https://gitlab.com/neurocomputing/myorobotic-arm && cd myorobotic-arm

# 2. Execute the modified NRP installer to setup NRP + requiremenets (this takes a while)
./nrp_installer.sh install
```

### Manual installation

1. Install/setup NRP

2. Follow the instructions for your distribution here: https://github.com/Roboy/musc-le_experiment

3. Install `rospy` (e. g. `apt install ros-melodic-rospy`)

4. Build the myo controlsby executing `catkin_make` in the `myo` directory

5. Ensure that the ros master node is running and accessible and that the variables for `$ROS_MASTER_URI`, `$ROS_IP` and `$PYTHONPATH` (latter is needed for rospy)

### Extra steps for simulation

To control the simulation you need to setup muscle parameters in ROS:

```bash
rosparam set /cardsflow_xml ~/.gazebo/models/musc_le/cardsflow.xml
```

## Usage

The library can work in _either_ simulation or physical mode.
We are imposing limitations to the motor controls, so you cannot break the robot with extreme values.

The framework can be used to either interface directly with the robot (low-level) or to execute an entire experiment directly.

The basic control structure is called a `Message` and is documented in the `message` module. Basically messages _from_ ROS are called `StatusMessage`s and contain `shoulder_angle`, `elbow_angle` and `position` (X, Y). Messages _to_ ROS are called `ControlMessage`s and simply require 4 floating point numbers (in Newton) to control the 4 motors.

**Note** that the forces in newton might be inaccurate in the physical robot due to assymetries in the motor position, spring dynamics etc.

### Message types

Motors are always enumerated such that

0. Shoulder joint, left motor
1. Shoulder joint, right motor
1. Elbow joint, left motor
1. Elbow joint, right motor

#### `ControlMessage`

Controls the joints by applying forces in Newton to each of the joints

```python
# Applies 10 N to elbow joint, left motor
controller = ...
msg = ControlMessage(0, 0, 10, 0)
controller.send(msg)
```

#### `StatusMessage`

Information about the joints and muscles

```Python
# Sets up a callback to the controller that receives StatusMessages
def callback(msg):
    msg.time # Time in ns
    msg.shoulder_angle # Angle
    msg.elbow_angle # Angle
    msg.position # Extreme point of the elbow joint in (X, Y) coordinates

Controller.create(callback, simulation=True)
```

### Simulation mode

The simulation mode requires a NRP simulation (ROS master) running.

### Hardware

The hardware mode requires access to the physical instance of myorobot. Contact the KTH team is this is necessary. In _principle_ the forces translates roughly 1:1 from simulation to physical system, but expect some jitter.

### Low-level usage: Creating a Controller

Two controllers exist: A `RobotController` and a `SimulationController`. To construct a controller, you need to create a callback for when messages are received from the robot

```Python
import control
callback = lambda msg: ...
controller = control.Controller.create(callback)
```

The callback will now receive messages from ROS automatically, and messages can be sent via the `.send` method:

```Python
# Sets the shoulder motors to (0, 1) and 
#   elbow motors to (2, 3), respectively
controller.send(0, 1, 2, 3)
```

### High-level usage: Implementing a model

The high-level controls are divided into three phases, see below.
To interact with the phases, a `Model` needs to be created, which basically just implements two
methods: `phase_motor_babbling` and `phase_throwing`. 
The two methods takes a trajectory as an input, and sends motor commands to the robot as output.

#### 1. Pick-up phase

- Hardcoded, disregard.

#### 2. Motor-babbling phase

A trajectory will generate a series of angles corresponding to a sinusoidal 'babbling' movement. The model picks up the angles and translates them into a force per moter, which is sent to the robot (simulation or physical). The robot provides feedback in terms of the actual forces applied (accounting for the inherent variations of the robot mechanics), velocity and position (joint angles). It is based on this feedback loop that the model learns.

```mermaid
stateDiagram
  TrajectoryGeneration --> Model : Angle (a, b)
  Model --> Robot : Forces
  Robot --> Model : Actual forces, velocity, joint angles
```

#### 3. Throwing phase

Similar to the motor-babbling phase, only the trajectory is now a throwing movement. The release of the object is included in the trajectory controls, and you can disregard it.

```mermaid
stateDiagram
  TrajectoryGeneration --> Model : Angle (a, b)
  Model --> Robot : Forces
  Robot --> Model : Actual forces, velocity, joint angles
```

### A note on (a)synchronicity

The control interface is receiving messages **asynchronously**, but sending them **synchronously**. 
Both the receiver and publisher are buffered, so this means that the messages are being received as fast
as you can cope with them, but sent as fast as your application can send them. 
Be careful here: the NRP is not the fastest beast on the planet, so it can mean that your commands are
being buffered for a long time before sent (= delayed). 
To mitigate this, the buffer size for the controller defaults to 10, and the frequency with which you are 
restricted to send to 100. 
These parameters can easily be changed, but be aware that they exist.

## Contact

If something is broken, please get in touch in the slack repository or by mail to `jeped@kth.se` and `jprb@kth.se`.
