# SpiNNaker board 48 node setup

This document describes the setup for a Spinn-5 board.

## Connectivity

The board comes with a power socket and two adjacent ethernet plugs. The ethernet plug closest to the edge is used for data transfer (and will likely be the only one we'll need to use; [read more here](https://spinnakermanchester.github.io/docs/)), and the other is used for hardware commands like resetting etc.

The board also has a ribbon cable sticking out on the same side as the ethernet ports. This is for composing the board with other boards and are not needed for our purpose.

## Setup

The internal IP of the board is `192.168.240.1` (for data transfer, the power interface in the other socket is `192.168.240.0`). 
So to communicate with the board you need to connect the board via ethernet and setup a network (e. g. `sudo ip a add 192.168.240.2/16 dev enp8s0`). Below is an example of a working `/etc/network/interfaces` entry:

```
iface eth1 inet static
address 192.168.240.2
gateway 0.0.0.0
netmask 255.255.0.0
```

Note the subnet mask of 16 bits. This is required to access the BMP on `.0`.
If the interface is correctly setup, you should be able to ping the board.

## Resetting

Sometimes the connection to the board is lost (`Failed to connect`, `Cannot establish connection`, similar). This is likely due to routing problems on the board or firewall issues. 
The fix for this is to either power cycle the machine (turn it off, turn it on), or, if that does not work, use the SpiNNaker tool `rig` (https://github.com/project-rig/rig). 
Assuming the board BMP address is `192.168.240.0`:

```python
pip install rig
rig-power 192.168.240.0
```

## Running experiments

Experiments are typically run via [sPyNNaker8](https://github.com/SpiNNakerManchester/sPyNNaker8), which is a [PyNN](http://neuralensemble.org/PyNN/) interface.  
Before you can run experiments with sPyNNaker8 you need a `.spynnaker.cfg` file in your home directory. The file contains configurations about the board, and is read by SpiNNaker at runtime.
If you retained the above configuration, the `.spynnaker.cfg.board` should be a [minimum working example (MWE)](https://en.wikipedia.org/wiki/Minimal_working_example). Simply copy it to your home folder like so:

```bash
cp .spynnaker.cfg.board ~/.spynnaker.cfg
```

For development we recommend using the [sPyNNaker8Jupyter](https://github.com/SpiNNakerManchester/sPyNNaker8Jupyter) project that runs SpiNNaker through a Jupyter notebook system.

Plenty of examples are available at the [PyNN8Examples repository](https://github.com/SpiNNakerManchester/PyNN8Examples).
