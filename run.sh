#!/bin/bash

##
## A script to run HBP containers
##

sudo /etc/init.d/supervisor start

while sleep 60; do
  ps aux | grep supervisord | grep -v grep
  STATUS=$?
  if [ $STATUS -ne 0 ] ; then
    echo "Supervisord exited"
    exit 1
  fi
done
