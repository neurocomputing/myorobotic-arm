SHELL := /bin/bash

.PHONY: setup build

all: setup build

build:
	sudo mkdir -p build devel
	sudo chown -R bbpnrsoa build devel
	catkin_make

run: build
	source devel/setup.bash && rosrun control main.py

setup:
	@sudo apt update
	@sudo apt install -y ros-${ROS_DISTRO}-desktop-full libeigen3-dev libxml2-dev coinor-libipopt-dev \
	  qtbase5-dev qtdeclarative5-dev qtmultimedia5-dev qml-module-qtquick2 \
	  qml-module-qtquick-window2 qml-module-qtmultimedia qml-module-qtquick-dialogs \
	  qml-module-qtquick-controls qml-module-qt-labs-folderlistmodel qml-module-qt-labs-settings \
	  ros-${ROS_DISTRO}-moveit-msgs doxygen swig mscgen ros-${ROS_DISTRO}-grid-map \
	  ros-${ROS_DISTRO}-controller-interface ros-${ROS_DISTRO}-controller-manager ros-${ROS_DISTRO}-aruco-detect \
	  ros-${ROS_DISTRO}-effort-controllers libxml++2.6-dev ros-${ROS_DISTRO}-robot-localization libalglib-dev \
	  ros-${ROS_DISTRO}-tf ros-${ROS_DISTRO}-interactive-markers ros-${ROS_DISTRO}-tf-conversions \
	  ros-${ROS_DISTRO}-robot-state-publisher
	@rm -rf ${HBP}/Experiments/musc-le_experiment && git clone https://github.com/Roboy/musc-le_experiment.git ${HBP}/Experiments/musc-le_experiment && cp -rf ${HBP}/Experiments/musc-le_experiment/* ${HBP}/Experiments/myoarm_nst && rm -rf ${HBP}/Experiments/musc-le_experiment
	@[ -d ${HBP}/GazeboRosPackages/src/cardsflow_gazebo ] || git clone https://github.com/CARDSflow/cardsflow_gazebo.git -b kth ${HBP}/GazeboRosPackages/src/cardsflow_gazebo
	@[ -d ${HBP}/GazeboRosPackages/src/common_utilities ] || git clone https://github.com/Roboy/common_utilities.git ${HBP}/GazeboRosPackages/src/common_utilities
	@[ -d ${HBP}/GazeboRosPackages/src/roboy_communication ] || git clone https://gitlab.com/neurocomputing/roboy_communication.git ${HBP}/GazeboRosPackages/src/roboy_communication
	@[ -d ${HBP}/GazeboRosPackages/src/musc-le_models ] || git clone https://github.com/Roboy/musc-le_models.git ${HBP}/GazeboRosPackages/src/musc-le_models
	@cd ${HBP}/GazeboRosPackages/ && catkin_make
	@cp -a ${HBP}/GazeboRosPackages/src/musc-le_models/musc_le ${HBP}/Models
	@cp -a ${HBP}/GazeboRosPackages/src/musc-le_models/musc_le_table ${HBP}/Models
	@cd ${HBP}/Models && ./create-symlinks.sh

cerebellum:
	@rosparam set /cardsflow_xml ~/.gazebo/models/musc_le/cardsflow.xml
	@pip install --upgrade pip
	@pip install pyrsistent==0.15.7 spynnaker8 numpy pynn==0.9.4 texttable
	@pip install -e src/control
	@sudo cp src/cerebellum/.spynnaker.local.cfg /home/bbpnrsoa/.spynnaker.cfg

reset:
	@rm -rf ${HBP}/Experiments/musc-le_experiment && git clone https://github.com/Roboy/musc-le_experiment.git ${HBP}/Experiments/musc-le_experiment && cp -rf ${HBP}/Experiments/musc-le_experiment/* ${HBP}/Experiments/myoarm_nst && rm -rf ${HBP}/Experiments/musc-le_experiment
	@rm -rf ${HBP}/GazeboRosPackages/src/cardsflow_gazebo && git clone https://github.com/CARDSflow/cardsflow_gazebo.git -b kth ${HBP}/GazeboRosPackages/src/cardsflow_gazebo
	@rm -rf ${HBP}/GazeboRosPackages/src/common_utilities && git clone https://github.com/Roboy/common_utilities.git ${HBP}/GazeboRosPackages/src/common_utilities
	@rm -rf ${HBP}/GazeboRosPackages/src/roboy_communication && git clone https://github.com/Roboy/roboy_communication.git -b kth ${HBP}/GazeboRosPackages/src/roboy_communication
	@rm -rf ${HBP}/GazeboRosPackages/src/musc-le_models && git clone https://github.com/Roboy/musc-le_models.git ${HBP}/GazeboRosPackages/src/musc-le_models
	@cd ${HBP}/GazeboRosPackages/ && catkin_make
	@cp -a ${HBP}/GazeboRosPackages/src/musc-le_models/musc_le ${HBP}/Models
	@cp -a ${HBP}/GazeboRosPackages/src/musc-le_models/musc_le_table ${HBP}/Models
	@cd ${HBP}/Models && ./create-symlinks.sh
	
