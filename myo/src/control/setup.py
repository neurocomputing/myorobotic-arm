from setuptools import setup

setup(
    name="control",
    version="0.1",
    description="Controls a myorobotic arm",
    author="Jens E. Pedersen",
    author_email="jeped@kth.se",
    package_dir={"": "src"},
    packages=[""],
    # Dependencies are rospy and spynnaker8
    # Assumed installed
)
