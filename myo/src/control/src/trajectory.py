"""
Module for providing trajectories as generators of points or angles over time.
"""
import math
import numpy as np


def sine_wave(start, dt=0.020, a1=1, a2=0.1, f1=0.2, f2=2.8):
    """
    Infinite generator for sine waves

    Keywoard Arguments
    ------------------
    start -- The initial input to the sine function
    dt    -- The change in timestep for the next iteration

    Returns an infinite generator
    """
    while True:

        val = (a1*math.sin(f1*start)+a2*math.sin(f2*start))/(a1+a2)
        yield val
        start += dt


def arc_wave(start, release, angle_step):
    """
    Infinite generator for throwing trajectory

    Keywoard Arguments
    ------------------
    start    -- The initial angle
    release  -- The release angle
    delta    -- The step at which the angle changes

    Returns an infinite generator
    """

    current = start
    while current > release:
        yield current
        current -= angle_step


def sine_generator(shoulder_start=0, elbow_start=0, dt=0.001):
    """
    Infinite sine-generator for shoulder and elbow angles

    Keywoard Arguments
    ------------------
    shoulder_start -- The initial input to the shoulder sine function
    elbow_start -- The initial input to the elbow sine function
    dt    -- The change in timestep for the next iteration

    Returns an infinite generator returning tuples of
    (shoulder_angle, elbow_angle)
    """
    shoulder_generator = sine_wave(shoulder_start, dt)
    elbow_generator = sine_wave(elbow_start, dt)
    while True:
        yield next(shoulder_generator), next(elbow_generator)


# TODO: trajectory_throwing/arc_generator under construction


def arc_generator(
    start_angle=math.pi * 55 / 180,
    release_angle=-math.pi * 19.700 / 180,
    angle_step=math.pi * 0.150 / 180,
    dt=0.001,
):
    """
    Parabol-generator for shoulder and elbow angles

    Keywoard Arguments
    ------------------
    dt    -- The change in timestep for the next iteration

    Returns ...
    """
    # The trajectory is hard-coded in such a way that:
    #    Angular Velocity is: 150.000[deg/s]
    #    Velocity: 3.048[m/s]
    #    Gamma: 32.290[deg]
    #    Target at: (-1.493,0.000)
    # using delta_t = 0.001
    shoulder_generator = arc_wave(start_angle, release_angle, angle_step)
    elbow_generator = arc_wave(start_angle, release_angle, angle_step)

    while True:
        try:
            next_shoulder = next(shoulder_generator)
            next_elbow = next(elbow_generator)
            yield next_shoulder, next_elbow
        except StopIteration:
            break
