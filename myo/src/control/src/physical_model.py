import math
import time

class PhysicalSetup:

    # The height of the joint in meters above the floor
    upper_joint_height = 0.90  # [m]

    # The length of the upper link (shoulder to elbow) in meters
    upper_link_length = 0.42  # [m]

    # The length of the lower link (elbow to end-effector) in meters
    lower_link_length = 0.38  # [m]

    # nb of ticks at upper joint sensor when pendulum at vertical equilibrium
    uj_ang_ticks = 2678

    # nb of ticks at lower joint sensor when pendulum at vertical equilibrium
    lj_ang_ticks = 2508

    # Max displacement allowed by springs ()
    max_displacement = 320

    # Max angle possible (in degrees)
    max_angle = 70

    # Estimated magnetic release release
    release_delay = 0.060  # [s]

    def __init__(self):

        self.time_p = -1  # time of previous update
        self.time_c = -1  # time of current update
        self.alpha = 0  # upper joint angle (shoulder)
        self.beta = 0  # lower joint angle (elbow)
        self.uj_w = 0  # upper joint angular velocity
        self.lj_w = 0  # lower joint angular velocity
        self.uj_x = 0  # upper joint x coordinate
        self.uj_y = 0  # upper joint y coordinate
        self.lj_x = 0  # lower joint x coordinate
        self.lj_y = 0  # lower joint y coordinate
        self.ee_x = 0  # end-effector x coordinate
        self.ee_y = 0  # end-effector y coordinate
        self.ee_v = 0  # end-effector linear velocity (object's velocity)

    def update_joints(self, alpha, beta, unit="ticks"):
        """
        Update arm data using joint positions.

        Keyword arguments
        -----------------
        alpha -- angular position of shoulder (upper joint)
        beta -- angular position of elbow (lower joint)
        unit -- 'ticks', 'degrees', 'radians'
        """
        # Register time of update
        if self.time_c == -1:
            # First update
            self.time_p = -1
            self.time_c = time.time()
            delta_t = float("inf")
        else:
            # Further updates
            self.time_p = self.time_c
            self.time_c = time.time()
            delta_t = self.time_c - self.time_p

        # Save old values (alpha, beta) to estimate angular velocity of joints
        old_alpha = self.alpha
        old_beta = self.beta

        # Save old values (X-Y) to estimate velocity of end-effector
        old_ee_x = self.ee_x
        old_ee_y = self.ee_y

        # Convert alpha/beta from 'ticks' or 'degrees' to 'radians'
        if unit == "ticks":
            self.alpha = (alpha - self.uj_ang_ticks) * 2.0 * math.pi / 4096.0
            self.beta = (beta - self.lj_ang_ticks) * 2.0 * math.pi / 4096.0
        elif unit == "degrees":
            self.alpha = alpha * math.pi / 180
            self.beta = beta * math.pi / 180
        else:  # radians
            self.alpha = alpha
            self.beta = beta

        # Estimate new values (X-Y) for upper/lower joint and end-*effector
        self.uj_x = 0
        self.uj_y = self.upper_joint_height
        self.lj_x = self.upper_link_length * math.sin(self.alpha)
        self.lj_y = self.upper_joint_height - self.upper_link_length * math.cos(
            self.alpha
        )
        self.ee_x = self.lj_x + self.lower_link_length * math.sin(
            self.alpha + self.beta
        )
        self.ee_y = self.lj_y - self.lower_link_length * math.cos(
            self.alpha + self.beta
        )

        # Estimate angular velocity for upper joint and lower joint
        self.uj_w = (self.alpha - old_alpha) / delta_t
        self.lj_w = (self.beta - old_beta) / delta_t

        # Estimate linear velocity in both axes (X-Y) for end-effector
        ee_vel_x = (self.ee_x - old_ee_x) / delta_t
        ee_vel_y = (self.ee_y - old_ee_y) / delta_t
        self.ee_v = math.sqrt(ee_vel_x ** 2 + ee_vel_y ** 2)
