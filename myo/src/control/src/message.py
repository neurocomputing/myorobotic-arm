"""
Messages for interacting with the robot, reading (StatusMessage) and writing (ControlMessage)
"""
import time
from roboy_middleware_msgs.msg import MotorCommand

import rospy


class Message(object):
    def __init__(self):
        self.time = time.clock()


class ControlMessage(Message):
    """
    Message to control the robot by sending values to the four motors
    """

    def __init__(self, values):
        """
        Creates a new command to assign the given values to the robot

        Keyword parameters:
        values -- The value to apply a given motor, ordered by
            1. Shoulder, left
            2. Shoulder, right
            3. Elbow, left
            4. Elbor, right
            The meaning of the value depends on the current control mode. See the control module.
        """
        super(ControlMessage, self).__init__()
        assert len(values) == 4, "4 forces expected, but " + str(len(values)) + " found"
        self.values = values

    def to_motor_command(self):
        return MotorCommand(id=3, motors=[0, 1, 2, 3], set_points=self.values)


class AttachMessage(Message):
    """
    Message to attach/detach object by sending values to 'magnet'
    """

    def __init__(self, obj_label):
        """
        Creates a new command to assign the given values to the robot

        Keyword parameters:
        obj_label -- label of object to pick-up
        """
        super(AttachMessage, self).__init__()
        assert len(obj_label) == 1, (
            "1 object expected, but " + str(len(action)) + " found"
        )
        self.obj_label = obj_label

    def to_object_attachment(self):
        if self.obj_label == "H":
            msg = "robot::cricket_ball_5000g"
            rospy.logdebug("Attaching 5kg")
        elif self.obj_label == "M":
            msg = "robot::cricket_ball_2000g"
            rospy.logdebug("Attaching 2kg")
        elif self.obj_label == "L":
            msg = "robot::cricket_ball_200g"
            rospy.logdebug("Attaching 200g")
        else:
            assert False, "Unidentified Object"
        return msg


class StatusMessage(Message):
    """
    Robot status in terms of 1) angles for should and elbow joints and 2) position of the elbow extremity (the point where the elbow ends)
    """

    def __init__(self, shoulder, elbow, time_previous, time_current):
        super(StatusMessage, self).__init__()
        self.shoulder = shoulder
        self.elbow = elbow
        self.time_previous = time_previous
        self.time_current = time_current

    def __str__(self):
        return ""

class LimbState:
    def __init__(self, angle, velocity, position):
        self.angle = angle
        self.velocity = velocity
        self.position = position

    def __str__(self):
        return ""
