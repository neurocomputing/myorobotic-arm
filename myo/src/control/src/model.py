from abc import ABCMeta, abstractmethod

from control import Controller
from message import ControlMessage, StatusMessage
import trajectory

import rospy
import time


class Model(object):

    __metaclass__ = ABCMeta

    """
    An abstract model which can perform three experiment phases:
      1. Pick up (hardcoded)
      2. Motor babbling
         Input: Trajectory of type T and StatusMessages
         Output: Control messages to the robot
      3. Throwing:
         Input: Throwing trajectory of type T and StatusMessages
         Output: Control messages to the robot
    """

    def __init__(self, control_mode="simulation", frequency=100, dt=0.0001):
        """
        Creates a model with a given controller, which specifies the necessary
        feedback callback from the robot

        Keyword arguments:
        control_mode -- Simulation ("simulation") or physical ("physical")
        babbling_trajectory -- A String indicating whether the babbling trajectory
                               should be angle or position driven
        throwing_trajectory -- A String indicating throwing trajectory to be used
        dt -- The trajectory timestep, defaults to 0.1
        """
        if control_mode == "simulation":
            simulation = True
        else:
            simulation = False

        self.babbling_trajectory = trajectory.sine_generator(dt=dt)
        self.throwing_trajectory = trajectory.arc_generator()

        self.controller = Controller.create(
            self.update, simulation, frequency=frequency
        )
        rospy.loginfo("Controller: " + str(self.controller))

    def phase_pick_up(self, obj_label):
        """
        Executes a picking-up phase where an object is attached to the tip of
        the arm. The object is picked at the same (x,y,z) coordinate every time.

        Keyword arguments:
        obj_label -- label of one of the available objects
                  "L", "M", "H", "X" indicate "light", "medium", "heavy", "unknown"
        """
        self.controller.send("magnet", "attach", obj_label)

    @abstractmethod
    def phase_motor_babbling(self, trajectory):
        """
        Executes a motor babbling phase where a given sinusoidal-like pattern
        is generated via the trajectory input.

        Keyword arguments:
        trajectory -- A generator generating a sinusoidal-like trajectory tuples in
                      the format [(shoulder_x, shoulder_y), (elbow_x, elbow_y)],
                      where x and y either represents angles or positions.
        """
        raise NotImplementedError()

    @abstractmethod
    def phase_throwing(self, trajectory):
        """
        Executes a throwing phase where the given trajectory represents
        the expected path of the robot.
        Note that this method should not be called directly, but as a part of an Experiment, since
        the trajectory *does not* include the release of the object.

        Keyword arguments:
        trajectory -- A generator generating a particular trajectory tuples in
                      the format [(shoulder_x, shoulder_y), (elbow_x, elbow_y)],
                      where x and y either represents angles or positions.
        """
        raise NotImplementedError()

    @abstractmethod
    def update(self, msg):
        """
        Receives an update message (sensory input) from the robot in the shape of a Message
        """
        raise NotImplementedError

    def stop(self):
        try:
            self.controller.unsubscribe()
        except:
            pass
