from control import Controller as C
from message import ControlMessage


def callback(x):
    print(x)


c = C.create(callback)

import time

time.sleep(1)
c.send(ControlMessage([0, 0, 0, 0]))
time.sleep(2)
c.send(100, 100, 100, 100)
