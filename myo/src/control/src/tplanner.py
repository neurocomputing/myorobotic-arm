import numpy as np
import matplotlib.pyplot as plt
import math
import time
import trajectory
from physical_model import PhysicalSetup

"""
This script is meant to be used when finding parameters release_angle and
angle_step for trajectory planning. The hyperparameters are: delta_t,
ideal_target, max_error, max_angle_step, start_angle, rho and eta.
"""

global t_resolution
global delta_t
global arm


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


"""
This function draws a predefined trajectory

Inputs:
- a vector of positions in x-axis for end-effector
- a vector of positions in y-axis for end-effector
- a vector of positions in x-axis for lower-joint
- a vector of positions in y-axis for lower-joint
- instant 't' at which release command is sent
- instant 't' at which actual release happens
- instant 't' at which object lands on target

Outputs:
None

"""


def draw_stuff(ee_x, ee_y, lj_x, lj_y, t_command, t_release, t_target):

    # These are the physical parameters for the robot
    uj_x = 0  # position of upper joint in x-axis
    uj_y = PhysicalSetup.upper_joint_height  # position of upper joint in y-axis
    ul_l = PhysicalSetup.upper_link_length  # lenth of link connected to upper joint
    ll_l = PhysicalSetup.lower_link_length  # lenth of link connected to lower joint

    fig, ax = plt.subplots()
    print("t_command = %3.3d" % (t_command))
    print("t_release = %3.3d" % (t_release))
    print("t_target = %3.3d" % (t_target))

    # Draw trajectory of Object before Release Command
    ax.plot(ee_x[0:t_release], ee_y[0:t_release], "r")

    if t_command < t_release:
        ax.plot(ee_x[t_command:t_release], ee_y[t_command:t_release], "m")

    # Draw trajectory of Object after Actual Release
    ax.plot(ee_x[t_release:t_target], ee_y[t_release:t_target], "b")

    # Draw Upper Joint
    ax.scatter(0, uj_y, color="k", marker="o")

    # Draw End Effector at Start point
    ax.scatter(ee_x[0], ee_y[0], color="k", marker="*")

    if t_command < t_release:
        ax.scatter(ee_x[t_command], ee_y[t_command], color="k", marker="*")

    # Draw End Effector at Actual Release point
    ax.scatter(ee_x[t_release], ee_y[t_release], color="k", marker="P")

    # Draw Target
    ax.scatter(ee_x[t_target], ee_y[t_target], color="k", marker="x")

    # Draw Arm and Lower Joint at Start point
    ax.plot([0, lj_x[0]], [uj_y, lj_y[0]], "g")
    ax.plot([lj_x[0], ee_x[0]], [lj_y[0], ee_y[0]], "g")
    ax.scatter(lj_x[0], lj_y[0], color="k", marker="o")

    # Draw Arm and Lower Joint at Release point
    ax.plot([0, lj_x[t_release]], [uj_y, lj_y[t_release]], "g")
    ax.plot([lj_x[t_release], ee_x[t_release]], [lj_y[t_release], ee_y[t_release]], "g")
    ax.scatter(lj_x[t_release], lj_y[t_release], color="k", marker="o")

    ax.grid(color="#eeefff", linestyle="--", linewidth=0.5)
    ax.set_aspect("equal")
    ax.set_xticks(np.arange(round(min(ee_x) - 1), round(max(ee_x)), 0.2))
    ax.set_yticks(np.arange(0, 2.5 * round(max(ee_y)), 0.2))
    ax.set_xlabel("X")
    ax.set_ylabel("Y")

    plt.show()


"""
This function prints information related to the trajectory planning

Inputs:
- the initial angle (same for both joints)
- the release angle (same for both joints)
- the angular change during 'delta_t'
- the time elapsed between updates
- the angular velocity (supposed to be constant)
- the velocity of the object when launched
- the x coordinate of the target

Outputs:
None
"""


def print_stuff(
    start_angle, release_angle, angle_step, delta_t, gamma, v_total, cur_target
):
    print(
        "Start Angle: %3.3f[deg] (%3.6f[rad])"
        % (start_angle * 180 / math.pi, start_angle)
    )
    print(
        "Release Angle: %3.3f[deg] (%3.6f[rad])"
        % (release_angle * 180 / math.pi, release_angle)
    )
    print(
        "Angle Step: %3.3f[deg] (%3.6f[rad])" % (angle_step * 180 / math.pi, angle_step)
    )
    print("Angular Velocity is: %3.3f[deg/s]" % (angle_step * 180 / math.pi / delta_t))
    print("'Launch' Velocity: %3.3f[m/s]" % (v_total))
    print("'Launch' Angle: %3.3f[deg]" % (gamma * 180 / math.pi))
    print("Target at: (%1.3f,%1.3f)" % (cur_target, 0))


"""
This function builds the trajectory followed by an object that is moved by a
2-joint arm. The motion starts at a certain angle and is described by a certain
velocity. The object is released and follows a parabolic trajectory until it hits
the ground.

Inputs:
- the initial angle for both joints
- the angular speed given by a delta angle and a delta time ('hardcoded')
- the release angle

Outputs:
- the (x,y) coordinates of the object and the elbow joint
- the'launch' angle and velocity of the object when release_delay
- time of release and time of landing of the object
- position in x axis where the object lands
"""


def build_trajectory(start_angle, release_angle, angle_step):

    global arm

    # Acceleration due to gravity
    g = 9.8  # [m/s**2]

    # These are arrays to store coordinates for end-effector and lower joint
    ee_x = []  # x-coordinate of end-effector
    ee_y = []  # y-coordinate of end-effector
    lj_x = []  # x-coordinate of lower joint
    lj_y = []  # y-coordinate of lower joint

    shoulder_generator = trajectory.arc_wave(start_angle, release_angle, angle_step)
    elbow_generator = trajectory.arc_wave(start_angle, release_angle, angle_step)

    # This part concerns the robot-driven motion
    while True:

        try:
            a = next(shoulder_generator)
            b = next(elbow_generator)
            arm.update_joints(a, b, "radians")
            lj_x.append(arm.lj_x)
            lj_y.append(arm.lj_y)
            ee_x.append(arm.ee_x)
            ee_y.append(arm.ee_y)

        except StopIteration:
            break

    # Instant when the Object is released
    t_release = len(ee_x) - 1

    # This part concerns the projectile motion
    ee_vel_x = (ee_x[t_release] - ee_x[t_release - 1]) / delta_t
    ee_vel_y = (ee_y[t_release] - ee_y[t_release - 1]) / delta_t
    v_total = math.sqrt(ee_vel_x ** 2 + ee_vel_y ** 2)
    gamma = math.atan(abs(ee_vel_y / ee_vel_x))

    while ee_y[-1] + ee_vel_y * t_resolution >= 0:
        ee_vel_y = ee_vel_y - g * t_resolution
        lj_x.append(lj_x[-1])
        lj_y.append(lj_y[-1])
        ee_x.append(ee_x[-1] + ee_vel_x * t_resolution)
        ee_y.append(ee_y[-1] + ee_vel_y * t_resolution)

    # Instant when the Object 'hits' the target
    t_target = len(ee_x) - 1

    cur_target = ee_x[t_target]

    return ee_x, ee_y, lj_x, lj_y, gamma, v_total, t_release, t_target, cur_target


"""
This functions gets, based on a set of hyperparameters, the angular speed and the
release angle that will define the right trajectory followed by the object to reach
a certain target

Inputs:
   start_angle:     initial angle for both joints
   eta:             delta for angle step
   rho:             delta for release angle
   delay:           delay between release command and actual release of object
   max_angle_step:  limit of angle step (angular velocity)
   ideal_target:    position (in x axis) where object should hit the ground
   max_error:       precision

Outputs:
- the angular speed given by a delta angle and a delta time ('hardcoded')
- the release angle
"""


def get_parameters(
    start_angle, eta, rho, delay, max_angle_step, ideal_target, max_error
):

    # Boolean to indicate if parameters have been found
    success = False

    # Loop involving angular Velocity
    angle_step = math.pi * rho / 180
    while True:

        # Loop involving minimum release angle
        release_angle = math.pi * eta / 180
        command_angle = angle_step * delay / delta_t
        while True:

            (
                ee_x,
                ee_y,
                lj_x,
                lj_y,
                gamma,
                v_total,
                t_release,
                t_target,
                cur_target,
            ) = build_trajectory(start_angle, release_angle - command_angle, angle_step)

            # @TODO: t_command needs to be an integer
            t_command = int(t_release - round(delay / delta_t, 0))

            # Check if it's time to stop the 'release_angle' loop
            if abs((ideal_target - cur_target) / ideal_target * 100) < max_error:
                success = True
                break
            # Check if the ideal target has been reached
            elif release_angle < -start_angle:
                break
            else:
                release_angle -= math.pi * eta / 180

        # Check if it's time to stop the 'angle_step' loop
        if success or angle_step > max_angle_step:
            break
        else:
            angle_step += math.pi * rho / 180

    if success:
        print_stuff(
            start_angle, release_angle, angle_step, delta_t, gamma, v_total, cur_target
        )
        draw_stuff(ee_x, ee_y, lj_x, lj_y, t_command, t_release, t_target)
    else:
        print("It was impossible to find a trajectory using these hyperparameters")

    return release_angle, angle_step


if __name__ == "__main__":

    global delta_t
    global t_resolution
    global arm

    # X-Y Coordinates of Arm's Lower-Joint and End-Effector at instant 't'
    arm = PhysicalSetup()

    # Time stuff
    delta_t = 0.001  # 10 ms
    t_resolution = 0.001  # 1 ms

    # These are the hyper-parameters that define the trajectory
    start_angle = math.pi * 55 / 180
    eta = 0.05  # delta for 'release_angle'
    rho = 0.05  # delta for 'angle_step'
    max_angle_step = math.pi * 1.0 / 180
    ideal_target = -1.5
    max_error = 1  # 1%

    # Get Parameters for the case where there isn't delay between release command
    # and actual release of the object
    delay = 0.000
    release_angle, angle_step = get_parameters(
        start_angle, eta, rho, delay, max_angle_step, ideal_target, max_error
    )

    # Get Parameters for the case where the delay between release command and
    # the actual release of the object is 20[ms]
    delay = 0.060
    release_angle, angle_step = get_parameters(
        start_angle, eta, rho, delay, max_angle_step, ideal_target, max_error
    )
