import abc
from enum import Enum
import rospy

from sensor_msgs.msg import JointState
from roboy_middleware_msgs.msg import (
    MotorCommand,
    MagnetCommand,
    JointStatus,
    MotorStatus,
)
from roboy_middleware_msgs.srv import ControlMode, ControlModeRequest
from roboy_cognition_msgs.srv import Talk
from std_srvs.srv import Trigger

from physical_model import PhysicalSetup
from message import ControlMessage, Message, LimbState, StatusMessage, AttachMessage

import math
import sys
import time


class Controller:
    """
    Abstract controller that can listen to (subscribe) and send (publish) messages to a simulation
    """

    def __init__(self):
        raise NotImplementedError("Abstract class")

    @staticmethod
    def create(
        callback, simulation=True, queue_size=1, frequency=100,
    ):
        if not callable(callback):
            raise ValueError("Callback must be callable")

        if simulation:
            return VirtualController(
                callback, queue_size=queue_size, frequency=frequency
            )
        else:
            return PhysicalController(
                callback, queue_size=queue_size, frequency=frequency
            )

    def send(self, cmd_type, msg):
        """
        Send a message to the robot asynchronously

        Keyword arguments
        -----------------
        cmd_type -- 'motor' or 'magnet'
        msg -- A ControlMessage
        """
        raise NotImplementedError()

    def set_mode(self, mode):
        """
        Set Control Mode

        Keyword arguments
        -----------------
        mode -- 0..3 for position, velocity, displacement and force
        """
        raise NotImplementedError()

    def set_frequency(self, frequency):
        """
        Set Frequency

        Keyword arguments
        -----------------
        frequency --
        """
        raise NotImplementedError()


class PhysicalController(Controller):
    """
    Controller for the physical myorobotic arm, using ROS as middleware
    """

    class ControlMode(Enum):
        """
        The available control modes for the physical robot
        """

        POSITION = 0
        VELOCITY = 1
        DISPLACEMENT = 2

    MOTOR_PUBLISH = "/roboy/middleware/MotorCommand/"
    JOINT_SUBSCRIBE = "/roboy/middleware/JointStatus"
    MOTOR_SUBSCRIBE = "/roboy/middleware/MotorStatus"
    MAGNET_PUBLISH = "/roboy/middleware/MagnetCommand/"
    CONTROL_MODE_SERVICE = "/roboy/shoulder_left/middleware/ControlMode"

    def __init__(self, callback, queue_size, frequency=100):

        self.callback = callback

        # Joint-based Sensory Input
        self.physical_setup = PhysicalSetup()
        self.joint_subscriber = rospy.Subscriber(
            self.JOINT_SUBSCRIBE, JointStatus, self.__joint_callback,
        )

        # Muscle-based Sensory Input
        self.motor_status = MotorStatus()
        self.motor_subscriber = rospy.Subscriber(
            self.MOTOR_SUBSCRIBE, MotorStatus, self.__motor_callback,
        )

        # Muscle activation
        self.modeselector = rospy.ServiceProxy(self.CONTROL_MODE_SERVICE, ControlMode)
        self.publisher_motor = rospy.Publisher(
            self.MOTOR_PUBLISH, MotorCommand, queue_size=queue_size
        )

        # Gripper Control
        self.publisher_magnet = rospy.Publisher(
            self.MAGNET_PUBLISH, MagnetCommand, queue_size=queue_size
        )

        # ROS-related Stuff
        self.ros_node = rospy.init_node("control", anonymous=True)
        self.rate = rospy.Rate(frequency)  # in Hz
        rospy.on_shutdown(self.__del__)

    def __del__(self):

        self.joint_subscriber.unregister()
        self.motor_subscriber.unregister()
        self.publisher_motor.unregister()
        self.publisher_magnet.unregister()
        self.modeselector.close()

    def __joint_callback(self, state):
        self.physical_setup.update_joints(state.absAngles[0], state.absAngles[1])
        shoulder = LimbState(
            angle=self.physical_setup.alpha,
            velocity=self.physical_setup.uj_w,
            position=(self.physical_setup.lj_x, self.physical_setup.lj_y),
        )
        elbow = LimbState(
            angle=self.physical_setup.beta,
            velocity=self.physical_setup.lj_w,
            position=(self.physical_setup.ee_x, self.physical_setup.ee_y),
        )
        msg = StatusMessage(
            shoulder, elbow, self.physical_setup.time_p, self.physical_setup.time_c
        )
        self.callback(msg)

    def __motor_callback(self, state):
        self.motor_status = state

    def send(self, cmd_type, *msg):
        """
        Send a message to the robot asynchronously

        Keyword arguments
        -----------------
        msg -- Either a ControlMessage or a list of 4 elements
        """
        if cmd_type == "motor":
            assert len(msg) > 0, "Null input not allowed"
            if isinstance(msg[0], list):
                msg = msg[0]

            if len(msg) > 3:
                # Limit max displacement
                msg[0] = min(self.physical_setup.max_displacement, msg[0])
                msg[1] = min(self.physical_setup.max_displacement, msg[1])
                msg[2] = min(self.physical_setup.max_displacement, msg[2])
                msg[3] = min(self.physical_setup.max_displacement, msg[3])
                msg = ControlMessage(msg)
            else:
                msg = msg[0]
            self.publisher_motor.publish(msg.to_motor_command())
        elif cmd_type == "magnet":
            if msg[0] == "attach":
                msg = MagnetCommand()
                msg.cmd = 2
                self.publisher_magnet.publish(msg)
                time.sleep(0.1)
                msg.cmd = 1
                self.publisher_magnet.publish(msg)
                time.sleep(0.1)
            elif msg[0] == "detach":
                msg = MagnetCommand()
                msg.cmd = 0
                self.publisher_magnet.publish(msg)
                time.sleep(0.1)

    def set_mode(self, mode):
        if mode.value in self.ControlMode._value2member_map_:
            print("Setting '%s' Control Mode" % (mode.name))
            c_mode_request = ControlModeRequest()
            c_mode_request.motor_id = [0, 1, 2, 3]
            c_mode_request.control_mode = mode.value
            self.modeselector(c_mode_request)
        else:
            print("Control Mode NOT valid")

    def set_frequency(self, frequency):
        self.rate = rospy.Rate(frequency)


class VirtualController(Controller):
    """
    Controller for the simulation, using ROS as middleware
    """

    class ControlMode(Enum):
        """
        The available control modes for the simulated robot
        """

        FORCE = 3

    MOTOR_PUBLISH = "/roboy/middleware/MotorCommand/"
    JOINT_SUBSCRIBE = "/joint_states"
    ATTACH_SERVICE = "/roboy/simulation/joint/attach"
    DETACH_SERVICE = "/roboy/simulation/joint/detach"

    def __init__(self, callback, queue_size=1, frequency=100):
        """
        Initialises a simulation controller

        Keyword Arguments:
        ------------------
        callback -- The callback method receiving sensory stimuli
        queue_size -- Message buffer size
        frequency -- ROS message rate in Hz
        """
        self.callback = callback

        # Joint-based Sensory Input
        self.physical_setup = PhysicalSetup()
        self.joint_subscriber = rospy.Subscriber(
            self.JOINT_SUBSCRIBE,
            JointState,
            self.__joint_callback,
            queue_size=queue_size,
            tcp_nodelay=True,
        )

        # Muscle-based Sensory Input
        # ... The NRP model does not provide this kind of feedback

        # Muscle activation
        self.publisher_motor = rospy.Publisher(
            self.MOTOR_PUBLISH, MotorCommand, queue_size=queue_size
        )

        # Gripper Control
        self.attacher = rospy.ServiceProxy(self.ATTACH_SERVICE, Talk, persistent=True)
        self.detacher = rospy.ServiceProxy(
            self.DETACH_SERVICE, Trigger, persistent=True
        )

        # ROS-related Stuff
        self.ros_node = rospy.init_node("control", anonymous=True)
        self.rate = rospy.Rate(frequency)  # in Hz
        rospy.on_shutdown(self.__del__)

    def __del__(self):
        self.publisher_motor.unregister()
        self.joint_subscriber.unregister()
        self.attacher.close()
        self.detacher.close()

    def __joint_callback(self, state):
        self.physical_setup.update_joints(state.position[2], state.position[3], unit="radians")
        shoulder = LimbState(
            angle=self.physical_setup.alpha,
            velocity=self.physical_setup.uj_w,
            position=(self.physical_setup.lj_x, self.physical_setup.lj_y),
        )
        elbow = LimbState(
            angle=self.physical_setup.beta,
            velocity=self.physical_setup.lj_w,
            position=(self.physical_setup.ee_x, self.physical_setup.ee_y),
        )
        msg = StatusMessage(
            shoulder, elbow, self.physical_setup.time_p, self.physical_setup.time_c
        )
        self.callback(msg)

    def send(self, cmd_type, *msg):
        """
        Send a message to the robot asynchronously

        Keyword arguments
        -----------------
        msg -- Either a ControlMessage or a list of 4 elements
        """
        if not rospy.is_shutdown():
            if cmd_type == "motor":
                assert len(msg) > 0, "Null input not allowed"
                if isinstance(msg[0], list):
                    msg = msg[0]

                if len(msg) > 3:
                    # Limit max displacement
                    msg[0] = min(self.physical_setup.max_displacement, msg[0])
                    msg[1] = min(self.physical_setup.max_displacement, msg[1])
                    msg[2] = min(self.physical_setup.max_displacement, msg[2])
                    msg[3] = min(self.physical_setup.max_displacement, msg[3])
                    msg = ControlMessage(msg)
                else:
                    msg = msg[0]
                self.publisher_motor.publish(msg.to_motor_command())
            elif cmd_type == "magnet":
                if msg[0] == "attach":
                    rospy.logdebug("Attach Object")
                    msg = AttachMessage(msg[1])
                    self.attacher(msg.to_object_attachment())
                elif msg[0] == "detach":
                    rospy.logdebug("Detach Object")
                    self.detacher()

    def set_mode(self, mode):
        if mode.value in self.ControlMode._value2member_map_:
            print("Setting '%s' Control Mode" % (mode.name))
            # The only mode available in simulation is FORCE ...
        else:
            print("Control Mode NOT valid")

    def set_frequency(self, frequency):
        self.rate = rospy.Rate(frequency)
