"""
This example
  1) Defines a model using angle trajectories
  2) Sets up an experiment for all three phases
  3) Runs the experiment

It serves as an example on how to both use the low-level controller
(using callback and .send), and the high-level Model interface.
"""
from control import Controller
import experiment
import trajectory
import model
import rospy

import time


class ExampleModel(model.Model):
    """
    An example implementation of the Model class ...

    """

    def update(self, msg):
        pass  # Ignore sensory input

    def phase_motor_babbling(self, trajectory):
        rospy.loginfo("Starting motor babbling phase")
        self.controller.set_mode(self.controller.ControlMode.DISPLACEMENT)

        # Try different displacement values (20, 40 ... 360 ... 40, 20)
        delta = 20
        sign = 1
        index = 1
        while index > 0:
            val = (index - 1) * delta
            self.controller.send("motor", [val, 0, 0, val])
            print("val = %6.6f" % (val))
            if index == 20:
                sign = -1
            index = index + sign
            time.sleep(1)

        rospy.loginfo("Done with motor babbling")

    def phase_throwing(self, trajectory):
        self.controller.send("magnet", "detach")


# Create a model for a simulated robot using sine controls
example_model = ExampleModel("physical", dt=1)
# Choose an Object to Pick Up
obj_label = "X"  # "X" : any object (only for physical model)
# Create a high-level 3-phase experiment
experiment = experiment.Experiment(example_model, obj_label)
# Run all 3 experiment phases
experiment.run()
