"""
This example
  1) Defines a model using angle trajectories
  2) Sets up an experiment for all three phases
  3) Runs the experiment

It serves as an example on how to both use the low-level controller
(using callback and .send), and the high-level Model interface.
"""
from control import Controller
import experiment
import trajectory
import model
import rospy

import time


class ExampleModel(model.Model):
    """
    An example implementation of the Model class, where the
    only implemented phase is the motor babbling, and where
    the sensory input is ignored.

    """

    def update(self, msg):
        pass  # Ignore sensory input

    def phase_motor_babbling(self, trajectory):
        rospy.loginfo("Starting motor babbling phase")

        # Since trajectory is an infinite generator, we have to loop
        start_time = time.time()
        loop_seconds = 10
        cmd_amp = 500

        # Set Control Model
        self.controller.set_mode(self.controller.ControlMode.FORCE)

        while time.time() - start_time < loop_seconds:
            # Trajectory is in the shape [(shoulder_x, shoulder_y), (elbow_x, elbow_y)]
            # This model uses angles, which are in the range [0;1], so we have to
            # convert them to 4 motor commands with forces (we chose the range [0, 100]):
            (shoulder, elbow) = next(trajectory)
            commands = [
                (shoulder + 1) * cmd_amp,
                (1 - shoulder) * cmd_amp,
                (elbow + 1) * cmd_amp,
                (1 - elbow) * cmd_amp,
            ]

            # We then send the commands onwards to the controller
            self.controller.send("motor", commands)

        self.controller.send("motor", [0, 0, 0, 0])

        # Note that this example uses no form of concurrency!
        rospy.loginfo("Done with motor babbling")

    def phase_throwing(self, trajectory):
        while True:
            try:
                (shoulder, elbow) = next(trajectory)
            except StopIteration:
                rospy.loginfo("Ready to detach object")
                break
        self.controller.send("magnet", "detach")
        self.controller.send("motor", [0, 0, 0, 0])


# Create a model for a simulated robot using sine controls
example_model = ExampleModel("simulation", dt=1)
# Choose an Object to Pick Up
obj_label = "L"  # "L" : Light Object
# Create a high-level 3-phase experiment
experiment = experiment.Experiment(example_model, obj_label)
# Run all 3 experiment phases
experiment.run()
