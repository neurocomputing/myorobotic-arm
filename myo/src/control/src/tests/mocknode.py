import pytest
import rospy
import threading
import time


class MockRosServer:
    def __init__(self, topic_subscribe, topic_publish, incoming_type, outgoing_type):
        self.incoming = []
        self.topic_subscribe = topic_subscribe
        self.topic_publish = topic_publish
        self.incoming_type = incoming_type
        self.outgoing_type = outgoing_type

    def __enter__(self):
        return self

    def __exit__(self, t, v, b):
        pass

    def start(self):
        self.node = rospy.init_node("mock", anonymous=False)
        self.subscriber = rospy.Subscriber(
            self.topic_subscribe, self.incoming_type, self.incoming.append,
        )
        self.publisher = rospy.Publisher(self.topic_publish, self.outgoing_type)
        rospy.rate(5)
        rospy.spin()

    def send(self, msg):
        self.publisher.publish(msg)
