import pytest
import rospy
import threading
import time

from roboy_middleware_msgs.msg import MotorCommand, MotorStatus

from ..control import Controller, VirtualController

from .mockserver import MockRosServer

# Integration test control subscription/publishing
def test_subscribe():
    messages = []
    controller = Controller.create(messages.append)
    subscriber = rospy.Subscriber(
        VirtualController.TOPIC_PUBLISH, MotorStatus, self.messages.append,
    )
