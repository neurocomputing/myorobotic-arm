"""
A module which describes the template of an entire experiment
"""
from abc import ABCMeta, abstractmethod
from enum import Enum

from model import Model

import rospy
import time


class Experiment:
    """
    The actual experiment which is executed in three phases
    """

    def __init__(self, model, obj_label):
        self.model = model
        self.obj_label = obj_label

    def run(self):
        """
        Executes the actual experiment in all phases by
          1) Position the arm to grab an object
          - Reset the arm to default resting state
          2) Execute Motor Babbling phase
          - Reset the arm to default resting state
          3) Execute Throwing trajectory phase
        """

        time.sleep(1)
        rospy.logdebug("Experiment pick up phase starting")
        self.model.phase_pick_up(self.obj_label)
        rospy.logdebug("Experiment babbling phase starting")
        self.model.phase_motor_babbling(self.model.babbling_trajectory)
        rospy.logdebug("Experiment throwing phase starting")
        self.model.phase_throwing(self.model.throwing_trajectory)
        time.sleep(1)
        rospy.loginfo("Experiment stops")
