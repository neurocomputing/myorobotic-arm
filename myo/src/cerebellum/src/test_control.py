from .brain_control import BrainController


def limb_error_right_test():
    actual = 0.1
    expected = 0.2
    assert BrainController.limb_error(actual, expected) == (0, 100.0)


def limb_error_left_test():
    actual = 0.2
    expected = 0.1
    assert BrainController.limb_error(actual, expected) == (100, 0.0)
