import spynnaker8 as s8


def __port_generator(n):
    while True:
        yield n
        n += 1


PORT_GENERATOR = __port_generator(29996)


def activate_reading(population, callback):
    port_number = next(PORT_GENERATOR)
    s8.external_devices.activate_live_output_for(
        population,
        database_notify_host="localhost",
        database_notify_port_num=port_number,
    )
    live_spikes_receiver = s8.external_devices.SpynnakerLiveSpikesConnection(
        receive_labels=[population.label], local_port=port_number, send_labels=None
    )
    live_spikes_receiver.add_receive_callback(population.label, callback)


def activate_writing(population, callback):
    port_number = next(PORT_GENERATOR)
    external_input_control = s8.external_devices.SpynnakerPoissonControlConnection(
        poisson_labels=[population.label], local_port=port_number
    )
    s8.external_devices.add_poisson_live_rate_control(
        population, database_notify_port_num=port_number
    )
    # Set callback to inject (write) poisson rates
    external_input_control.add_start_resume_callback(population.label, callback)
