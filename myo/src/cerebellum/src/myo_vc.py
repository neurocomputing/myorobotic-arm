
import sys
import traceback
import logging
import time

from multiprocessing import Process, Queue
from hbp_nrp_virtual_coach.virtual_coach import VirtualCoach



def logMe(message):
    print("\n\n\n\n\n" + message + "\n\n\n\n\n")

'''
This script ('myo_vc.py') is launched in parallel with 'main.py'
Both scripts are launched by 'launch_all.sh'
In order to allow synchronisation, the file 'sim_status.mem' is used
'''
def start_nrp_simulation():
    logMe("Starting Virtual Coach")
    vc = VirtualCoach(environment='local', storage_username='nrpuser', storage_password='password')
    vc.print_cloned_experiments()
    sim = vc.launch_experiment('myoarm_nst_0')
    logMe("Starting NRP Simulation")
    sim.start()
    dt = 1
    t_wait = 2*3600
    idx = 0
    while idx < t_wait:
        f = open("sim_status.mem", 'r')
        status = f.readline()
        f.close()
        if status.find("stopped") >= 0:
            break
        time.sleep(dt)
        idx += dt
    logMe("Stopping NRP Simulation")
    sim.stop()
    time.sleep(5)

'''
A process is created in which an instance of the virtual coach launches then
experiment 'myo_nst_1' on the NRP.
'''
if __name__ == "__main__":

    nrp_sim_process = Process(target=start_nrp_simulation)
    nrp_sim_process.daemon = True
    try:
        nrp_sim_process.start()
        nrp_sim_process.join()
    except Exception as e:
        logging.error("Main terminated unexpectedly: " + str(e))
    except BaseException as e:
        logging.info("Escape")
    finally:
        logging.info("Stopping NRP thingy")
        nrp_sim_process.terminate()
        del nrp_sim_process
