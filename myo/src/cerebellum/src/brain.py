import math
import numpy as np
from multiprocessing import Process, Queue

import spynnaker8 as s8


import remote
import util
import coding
import time
import math
import record
import random
import logging
import queue

import rospy
import traceback
import csv

# def putPopData(PopName="", data=0, header=False):
#     try:
#         csvfile = open('results/' + PopName + '.csv', 'a+')
#         fieldnames = ['Time', 'Value']
#         writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
#         writer.writerow({'Time': time.time(), 'Value': data})
#         csvfile.close()
#     except Exception as e:
#         pass

def getParamVal(ParamName=""):
    with open('params_c0.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if ParamName in row['Parameter']:
                value = float(row['Value'])
                print(ParamName + ": " + str(value))
            # print(row['Parameter'], row['Value'])
    return value

# Set the number of neurons that are related to live in/out action
inout_pop_size = getParamVal(ParamName="inout_pop_size")

# cf_share + mf_share + dcn_share = 1
mf_share = getParamVal(ParamName="mf_share")
cf_share = (1-mf_share)*2/3
dcn_share = (1-mf_share)*1/3

mf_pop_size = int(inout_pop_size/4*mf_share)
cf_pop_size = int(inout_pop_size/4*cf_share)
dcn_pop_size = int(inout_pop_size/2*dcn_share)

pc_pop_size = cf_pop_size
gc_pc_ratio = getParamVal(ParamName="gc_pc_ratio")
gc_pop_size = pc_pop_size*gc_pc_ratio

print("\n\n\n\n\n")
print("mf_pop_size:" + str(mf_pop_size))
print("gc_pop_size:" + str(gc_pop_size))
print("pc_pop_size:" + str(pc_pop_size))
print("cf_pop_size:" + str(cf_pop_size))
print("dcn_pop_size:" + str(dcn_pop_size))
print("total_input::" + str(cf_pop_size*4+mf_pop_size*4+dcn_pop_size*2))
print("\n\n\n\n\n")



def _input_callback(population, queue, encoder, writer=None):
    def inner_callback(label, connection):
        try:
            while True:
                raw_value = queue.get()
                if raw_value < 0:
                    rospy.logerr("Cannot set values below 0 for " + str(label))
                    rospy.logerr("raw_value = " + str(raw_value))
                else:
                    coded_value = encoder(raw_value)
                    connection.set_rates(label, coded_value)
                    if writer:
                        writer(raw_value)
                        # putPopData(writer.name, raw_value)
        except Exception as e:
            rospy.logerr("Error when writing to brain: " + str(type(e)))
            rospy.logerr(traceback.format_exc())

    # Enable writing and observe the following callback
    remote.activate_writing(population, inner_callback)


def _input_population(
    label,
    queue,
    writer,
    encoding=coding.PopulationEncoding,
    size=100,
    source=s8.SpikeSourcePoisson(rate=0),
):
    pop = s8.Population(size, source, label=label)
    _input_callback(pop, queue, encoding(size), writer=writer.scalar(label))
    return pop

# Synapse helpers
def _syn_static(w):
    return s8.StaticSynapse(weight=w, delay=1.0)


def _syn_stdp(weight):
    return s8.STDPMechanism(
        timing_dependence=s8.SpikePairRule(
            tau_plus=40.0, tau_minus=40.0, A_plus=0.01, A_minus=0.012
        ),
        weight_dependence=s8.AdditiveWeightDependence(w_min=-0.01, w_max=0.05),
        delay=1.0,
        weight=weight,
    )

class OutputCallbackHandler:

    def __init__(self):
        self.callbacks = []
        self.populations = []

    def add_callback(self, population, decoder, message_queue=None, writer=None):
        self.populations.append(population)
        self.callbacks.append(self._output_callback(decoder, message_queue, writer))

    def _output_callback(self, decoder, message_queue=None, writer=None):
        def inner_callback(label, time_w, neuron_ids):
            try:
                value = decoder(len(neuron_ids), time_w)
                if message_queue:
                    message_queue.put(value, False)  # Do not wait/block
                if writer:
                    writer(value)
                    # putPopData(writer.name, value)
            except queue.Full:
                pass
            except Exception as e:
                rospy.logerr("Error when reading brain output: " + str(type(e)))
                rospy.logerr(traceback.format_exc())

        return inner_callback

    def setup_callbacks(self):
        labels = [x.label for x in self.populations]
        live_spikes_receiver = s8.external_devices.SpynnakerLiveSpikesConnection(
            receive_labels=labels, local_port=None, send_labels=None
        )
        for index, population in enumerate(self.populations):
            s8.external_devices.activate_live_output_for(
                population, database_notify_port_num=live_spikes_receiver.local_port
            )
            callback = self.callbacks[index]
            live_spikes_receiver.add_receive_callback(population.label, callback)

class LimbStateNetwork:
    """
    Represents the input state network for a single limb
    """

    def __init__(self, limb, pcs, dcns, writer):
        name = limb.name

        ##
        # Setup populations
        # Abbreviations:
        ##   a (angle), v (velocity), l (left), r (right)
        ##
        # Input population (MF)
        self.pop_mf_a_actual = _input_population(
            name + "_mf_a_actual", limb.actual, writer, size=mf_pop_size
        )
        self.pop_mf_a_desired = _input_population(
            name + "_mf_a_desired", limb.desired, writer, size=mf_pop_size
        )
        self.pop_mf_v_actual = _input_population(
            name + "_mf_v_actual", limb.actual_v, writer, size=mf_pop_size
        )
        self.pop_mf_v_desired = _input_population(
            name + "_mf_v_desired", limb.desired_v, writer, size=mf_pop_size
        )

        # State population (GC)
        self.pop_gc_a_actual = s8.Population(
            gc_pop_size, s8.IF_curr_exp(), label=name + "_gc_a_actual"
        )
        self.pop_gc_a_desired = s8.Population(
            gc_pop_size, s8.IF_curr_exp(), label=name + "_gc_a_desired"
        )
        self.pop_gc_v_actual = s8.Population(
            gc_pop_size, s8.IF_curr_exp(), label=name + "_gc_v_actual"
        )
        self.pop_gc_v_desired = s8.Population(
            gc_pop_size, s8.IF_curr_exp(), label=name + "_gc_v_desired"
        )

        # MF -> GC
        w_mf_gc = getParamVal(ParamName="w_mf_gc")
        s8.Projection(self.pop_mf_a_actual, self.pop_gc_a_actual, s8.AllToAllConnector(), _syn_static(w_mf_gc))
        s8.Projection(self.pop_mf_a_desired, self.pop_gc_a_desired, s8.AllToAllConnector(),_syn_static(w_mf_gc))
        s8.Projection(self.pop_mf_v_actual, self.pop_gc_v_actual, s8.AllToAllConnector(),_syn_static(w_mf_gc))
        s8.Projection(self.pop_mf_v_desired, self.pop_gc_v_desired, s8.AllToAllConnector(),_syn_static(w_mf_gc))

        # # MF -> DCN
        w_mf_dcn = getParamVal(ParamName="w_mf_dcn")
        for dcn in dcns:
            s8.Projection(self.pop_mf_a_actual, dcn, s8.AllToAllConnector(),_syn_static(w_mf_dcn))
            s8.Projection(self.pop_mf_a_desired, dcn, s8.AllToAllConnector(),_syn_static(w_mf_dcn))
            s8.Projection(self.pop_mf_v_actual, dcn, s8.AllToAllConnector(),_syn_static(w_mf_dcn))
            s8.Projection(self.pop_mf_v_desired, dcn, s8.AllToAllConnector(),_syn_static(w_mf_dcn))
        # GC -> PC (STDP)
        w_gc_pc = getParamVal(ParamName="w_gc_pc")
        self.pc_conn = []
        for pc in pcs:
            c1 = s8.Projection(self.pop_gc_a_actual, pc, s8.AllToAllConnector(),_syn_stdp(weight=w_gc_pc))
            c2 = s8.Projection(self.pop_gc_a_desired, pc, s8.AllToAllConnector(),_syn_stdp(weight=w_gc_pc))
            c3 = s8.Projection(self.pop_gc_v_actual, pc, s8.AllToAllConnector(),_syn_stdp(weight=w_gc_pc))
            c4 = s8.Projection(self.pop_gc_v_desired, pc, s8.AllToAllConnector(),_syn_stdp(weight=w_gc_pc))
            self.pc_conn.append((c1, c2, c3, c4))

        rospy.loginfo("Done with synaptic connections")


class LimbErrorNetwork:
    def __init__(self, callback_handler, output_network, limb, writer):
        name = limb.name
        # Error populations (CF)
        self.pop_cf_l_pull = _input_population(
            name + "_cf_l_pull",
            limb.error_left_pull,
            writer,
            size=cf_pop_size,
            encoding=coding.RateEncoding,
        )
        self.pop_cf_l_relax = _input_population(
            name + "_cf_l_relax",
            limb.error_left_relax,
            writer,
            size=cf_pop_size,
            encoding=coding.RateEncoding,
        )
        self.pop_cf_r_pull = _input_population(
            name + "_cf_r_pull",
            limb.error_right_pull,
            writer,
            size=cf_pop_size,
            encoding=coding.RateEncoding,
        )
        self.pop_cf_r_relax = _input_population(
            name + "_cf_r_relax",
            limb.error_right_relax,
            writer,
            size=cf_pop_size,
            encoding=coding.RateEncoding,
        )

        # Hidden population (PC)
        self.pop_pc_l_pull = s8.Population(pc_pop_size, s8.IF_curr_exp(), label=name + "_pc_l_pull")
        # self.pop_pc_l_relax = s8.Population(pc_pop_size, s8.IF_curr_exp(), label=name + "_pc_l_relax")
        self.pop_pc_r_pull = s8.Population(pc_pop_size, s8.IF_curr_exp(), label=name + "_pc_r_pull")
        # self.pop_pc_r_relax = s8.Population(pc_pop_size, s8.IF_curr_exp(), label=name + "_pc_r_relax")

        # CF -> PC
        w_cf_pc = getParamVal(ParamName="w_cf_pc")
        s8.Projection(self.pop_cf_l_pull, self.pop_pc_l_pull, s8.OneToOneConnector(), _syn_static(w_cf_pc))
        # _connect("one",self.pop_cf_l_relax, self.pop_pc_l_relax, _syn_static(w_cf_pc))
        s8.Projection(self.pop_cf_r_pull, self.pop_pc_r_pull, s8.OneToOneConnector(), _syn_static(w_cf_pc))
        # _connect("one",self.pop_cf_r_relax, self.pop_pc_r_relax, _syn_static(w_cf_pc))
        # CF -> DCN
        w_cf_dcn = getParamVal(ParamName="w_cf_dcn")
        s8.Projection(self.pop_cf_l_pull, output_network.pop_dcn_l, s8.OneToOneConnector(), _syn_static(w_cf_dcn))
        s8.Projection(self.pop_cf_r_pull, output_network.pop_dcn_r, s8.OneToOneConnector(), _syn_static(w_cf_dcn))
        # CF -> DCN relax
        w_cf_dcn_relax = getParamVal(ParamName="w_cf_dcn_relax")
        s8.Projection(self.pop_cf_l_relax, output_network.pop_dcn_l, s8.OneToOneConnector(), _syn_static(w_cf_dcn_relax))
        s8.Projection(self.pop_cf_r_relax, output_network.pop_dcn_r, s8.OneToOneConnector(), _syn_static(w_cf_dcn_relax))
        # PC -> DCN
        w_pc_dcn = getParamVal(ParamName="w_pc_dcn")
        s8.Projection(self.pop_pc_l_pull, output_network.pop_dcn_l, s8.OneToOneConnector(), _syn_static(w_pc_dcn))
        # _connect("one",self.pop_pc_l_relax, output_network.pop_dcn_l, _syn_static(w_pc_dcn_relax))
        s8.Projection(self.pop_pc_r_pull, output_network.pop_dcn_r, s8.OneToOneConnector(), _syn_static(w_pc_dcn))
        # _connect("one",self.pop_pc_r_relax, output_network.pop_dcn_r, _syn_static(w_pc_dcn_relax))


class LimbOutputNetwork:
    def __init__(self, callback_handler, limb, writer):
        name = limb.name
        # Output populations (DCN)
        self.pop_dcn_l = s8.Population(dcn_pop_size, s8.IF_curr_exp(), label=name + "_dcn_l")
        self.pop_dcn_r = s8.Population(dcn_pop_size, s8.IF_curr_exp(), label=name + "_dcn_r")

        # Enable reading by observing the following population onto the callback
        window_t = int(getParamVal("window_t"))
        callback_handler.add_callback(self.pop_dcn_l,
            coding.RateDecoding(window_time=window_t), message_queue=limb.out_left, writer=writer.scalar(name + "_dcn_l_both"))

        callback_handler.add_callback(self.pop_dcn_r,
            coding.RateDecoding(window_time=window_t), message_queue=limb.out_right, writer=writer.scalar(name + "_dcn_r_both"))


class Brain:
    """
    Models and simulates a cerebellum on SpiNNaker in real-time


    Architecture:

                                 CF_l        CF_r
                                   |          |
                                   |          |
                                   |          |
                          STDP     |          |
    GC_a --------------+-----------|----/\    |
      |                |           |   /  \   |
      |   GC_v---------+           |  /-  -\  |
      |    |                       |-        -|
      |    |                      PC_l      PC_r
      |    |                       |          |
      |----|---------+-------------|----/\    |
      |    |         |             |   /  \   |
     MF_a  |         |            = | /-  -\  |
           |         |             |-        -|
          MF_v-------+           DCN_l      DCN_r

    Usage:
    >>> brain = Brain(upper_arm)
    >>> brain.start()
    """

    def __init__(self, upper_arm):
        # Setup Tensorboard recording
        # See https://github.com/lanpa/tensorboardX/blob/master/tensorboardX/writer.py#L1035
        writer = record.Recorder(
            layout={
                "CFs": {
                    "upper": [
                        "Multiline",
                        [
                            "Upper_cf_l_pull",
                            "Upper_cf_l_relax",
                            "Upper_cf_r_pull",
                            "Upper_cf_r_relax",
                        ],
                    ]
                },
                "MFs": {
                    "upper": [
                        "Multiline",
                        [
                            "Upper_mf_a_actual",
                            "Upper_mf_a_desired",
                            "Upper_mf_v_actual",
                            "Upper_mf_v_desired",
                        ],
                    ]
                },
                "DCNs": {
                    "upper": ["Multiline", ["Upper_dcn_l_both", "Upper_dcn_r_both"]]
                },
            }
        )

        # Setup SpiNNaker experiment
        s8.setup(timestep=1.0, min_delay=1.0, max_delay=144.0)
        s8.set_number_of_neurons_per_core(s8.IF_curr_exp, 32)

        # Prepare callbacks
        callback_handler = OutputCallbackHandler()

        # Setup output and error networks and connections
        shoulder_output = LimbOutputNetwork(callback_handler=callback_handler,
            limb=upper_arm, writer=writer)
        shoulder_error = LimbErrorNetwork(
            callback_handler=callback_handler, output_network=shoulder_output,
            limb=upper_arm, writer=writer
        )

        # Setup state networks and connections
        dcns = [shoulder_output.pop_dcn_l, shoulder_output.pop_dcn_r]
        # pcs = [shoulder_error.pop_pc_l_pull, shoulder_error.pop_pc_l_relax,
        #        shoulder_error.pop_pc_r_pull, shoulder_error.pop_pc_r_relax]
        pcs = [shoulder_error.pop_pc_l_pull, shoulder_error.pop_pc_r_pull]
        shoulder_state = LimbStateNetwork(
            limb=upper_arm, pcs=pcs, dcns=dcns, writer=writer
        )
        self.shoulder_state = shoulder_state

        # Setup all callbacks on SpiNNaker
        callback_handler.setup_callbacks()

    def run(self):
        w = []
        try:
            # Prepare weights
            w = []
            for idx, conn in enumerate(self.shoulder_state.pc_conn):
                for c in conn:
                    w.append(c.get("weight", format="array"))

            # Run for the sim_duration
            sim_duration = 1000*int(getParamVal("sim_duration"))
            s8.run(sim_duration)
        except Exception as e:
            import traceback

            rospy.logerr("Brain terminated with exception")
            rospy.logerr(traceback.format_exc())
        finally:
            np.save("weights.npy", np.array(w))
            self.stop()

    def stop(self):
        try:
            s8.external_devices.request_stop()
            s8.end()
            rospy.loginfo("Brain ending")
        except s8.ConfigurationException:
            pass  # Simulation already terminated

if __name__ == "__main__":
    from limb import Limb
    arm = Limb("Bob")
    brain = Brain(arm)
    brain.run()