#!/usr/bin/env python

import sys
import traceback
import queue
import logging
from multiprocessing import Process, Queue
import time
import math

from experiment import Experiment
from brain import Brain
from brain_control import BrainController
import util
from limb import Limb

logging.basicConfig(level=logging.DEBUG)


def start_brain(upper_arm):
    import rospy

    try:
        brain = Brain(upper_arm)
        brain.run()
        rospy.logerr("Brain ended")
    except Exception as e:
        rospy.logerr("Brain terminated unexpectedly: " + str(e))
        rospy.logerr(traceback.format_exc())


def start_controller(upper_arm):
    import rospy

    try:
        #time.sleep(30)
        controller = BrainController(upper_arm, frequency=100, dt=0.01)
        experiment = Experiment(controller, "L")  # Light object
        experiment.run()
        rospy.logerr("Experiment ended")
    except Exception as e:
        rospy.logerr("Controller terminated unexpectedly: " + str(e))
        rospy.logerr(traceback.format_exc())


if __name__ == "__main__":
    # Start both brain and controller processes
    upper_arm = Limb("Upper")

    # Create a model for a simulated robot using angle controls
    brain_process = Process(target=start_brain, args=(upper_arm,),)
    brain_process.daemon = True
    control_process = Process(target=start_controller, args=(upper_arm,),)
    control_process.daemon = True

    try:
        start_time = time.time()
        brain_process.start()
        control_process.start()
        brain_process.join()
    except Exception as e:
        logging.error("Main terminated unexpectedly: " + str(e))
        logging.error(traceback.format_exc())
    except BaseException as e:
        logging.info("Escape")
    finally:
        logging.info("Stopping experiment")
        control_process.terminate()
        brain_process.terminate()
        upper_arm.close()
        logging.info("Done after %3.3f seconds" %(time.time() - start_time))
