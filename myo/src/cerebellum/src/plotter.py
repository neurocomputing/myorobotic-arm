#!/usr/bin/env python
import matplotlib.pyplot as plt
import sys
import time
import math
import numpy as np
from numpy import genfromtxt

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

import csv
import glob
import os

colors = ["#00CC00","#0000FF","#FF0000","#F9C908","#6600CC","#009999","#33FFFF","#FF007F","#202020","#FF8000"]

def plotPopData(Path="",PopName="", Axis=None, Ini=0, End=30):
    lines = []
    for filename in glob.glob(os.path.join(Path, '*.csv')):
        label = filename
        label = label.replace(Path,'')
        label = label.replace('.csv','')
        if "params" in filename:
            pass
        else:
            csvfile = open(filename, 'r') # open in readonly mode
            datmat = 0
            datmat = genfromtxt(csvfile, delimiter=',')
            idx_first = 0
            idx_last = 0
            for i in datmat[:,0]:
                if i < datmat[0,0] + Ini:
                    idx_first += 1
                if i < datmat[0,0] + End:
                    idx_last += 1
            if PopName in label:
                current_line, = Axis.plot(datmat[idx_first:idx_last,0]-datmat[0,0], datmat[idx_first:idx_last,1], label=label, linewidth=1)
                lines.append(current_line)
            csvfile.close()
    leg = Axis.legend(loc='center left', ncol=1, bbox_to_anchor=(1.01, 0.5));
    return lines, leg

#  python3 plotter.py summary/last/
if __name__ == "__main__":

    path = sys.argv[1]

    fig, (axs1, axs2, axs3) = plt.subplots(3, sharex=True, sharey=False, gridspec_kw={'hspace': 0})
    fig.set_size_inches((24, 12))

    title = "PARAMETERS\n"
    sim_duration = float("inf")
    filename = path+"params_c0.csv"
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            title += row['Parameter'] + ": " + row['Value'] + " | "
            if "sim_duration" in row['Parameter']:
                sim_duration = float(row['Value'])
    plt.suptitle(title)


    try:
        start_t = float(sys.argv[2])
        stop_t = float(sys.argv[3])
    except:
        start_t = sim_duration - 90
        stop_t = sim_duration

    lines1, leg1 = plotPopData(Path=path, PopName="cf", Axis=axs1, Ini=start_t, End=stop_t)
    lines2, leg2 = plotPopData(Path=path, PopName="mf", Axis=axs2, Ini=start_t, End=stop_t)
    lines3, leg3 = plotPopData(Path=path, PopName="dcn", Axis=axs3, Ini=start_t, End=stop_t)


    # activating Toggle
    lined = dict()
    for legline, origline in zip(leg1.get_lines(), lines1):
        legline.set_picker(5)  # 5 pts tolerance
        lined[legline] = origline
    for legline, origline in zip(leg2.get_lines(), lines2):
        legline.set_picker(5)  # 5 pts tolerance
        lined[legline] = origline
    for legline, origline in zip(leg3.get_lines(), lines3):
        legline.set_picker(5)  # 5 pts tolerance
        lined[legline] = origline


    def onpick(event):
        # on the pick event, find the orig line corresponding to the
        # legend proxy line, and toggle the visibility
        legline = event.artist
        origline = lined[legline]
        vis = not origline.get_visible()
        origline.set_visible(vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        if vis:
            legline.set_alpha(1.0)
        else:
            legline.set_alpha(0.2)
        fig.canvas.draw()

    fig.canvas.mpl_connect('pick_event', onpick)

    plt.show()
