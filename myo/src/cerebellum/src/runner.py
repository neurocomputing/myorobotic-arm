import os
import re
import shutil
import pdb
from shutil import copyfile



class ParamStuff(object):
    def __init__(self, name=""):
        self.name = name
        self.value = []
        self.c_count = 0

    def add(self, value=[]):
        self.value.append(value)
        self.c_count += 1



def loadParams(path="params.csv", delimiter=","):
    ParamList = []
    f = open(path, "r")
    for stringy in f:
        idx_colon = stringy.find(':')
        name=stringy[0:idx_colon]
        ParamList.append(ParamStuff(name))
        stringy = stringy[idx_colon+2:-1]
        idx_left = 0
        idx_right = len(stringy)
        for str_val in re.finditer(delimiter, stringy):
            ParamList[-1].add(float(stringy[idx_left: str_val.start()]))
            idx_left = str_val.end()
        ParamList[-1].add(float(stringy[idx_left: idx_right]))
    f.close()
    return ParamList



def printParams(paramlist=[]):
    for param in paramlist:
        print(param.name)
        print("c Count: " + str(param.c_count))
        for val in param.value:
            print("   " + str(val))

def createFiles(ParamList=[]):

    path_a = "touse/"
    path_z = "used/"
    try:
        shutil.rmtree(path_a)
        shutil.rmtree(path_z)
    except:
        pass
    os.mkdir(path_a)
    os.chmod(path_a,0o777)
    os.mkdir(path_z)
    os.chmod(path_z,0o777)

    f = open(path_a + "params_c0.csv", "a")
    f.write('Parameter,Value\n')
    f.close()


    # Create Files
    k = 0
    l = len(ParamList)
    # For each param
    for i in range(0,l):
        n = len(ParamList[i].value)
        listdir = os.listdir(path_a)
        for filename in listdir:
            for j in range(0,n):
                cfn = filename.replace('.csv', '')
                cfn = cfn + str(j) + '.csv'
                copyfile(path_a + filename, path_a + cfn)
                f = open(path_a + cfn, "a")
                f.write(ParamList[i].name + ',' + str(ParamList[i].value[j])+ '\n')
                f.close()
            os.remove(path_a + filename)

    # Rename Files
    idx = 1
    listdir = os.listdir(path_a)
    for filename in listdir:
        os.rename(path_a + filename, path_a + 'params_v' + str(idx) + '.csv')
        idx += 1

if __name__ == "__main__":

    createFiles(loadParams())
