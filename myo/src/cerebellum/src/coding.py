import math
import time
import rospy
import util


class PopulationEncoding:
    def __init__(self, size, scale=1):
        self.size = size
        self.scale = scale

    def __call__(self, value):
        return [(i, self.rbf(i, value)) for i in range(self.size)]

    def rbf(self, i, x):
        # e ^ (-(x - m)^2) / (2s^2) => [0;1]
        return math.exp(-((x - float(i) / self.size) ** 2) / 0.04) * self.scale


class RateEncoding:
    def __init__(self, size):
        self.size = size

    def __call__(self, value):
        return [(i, value) for i in range(self.size)]


class RateDecoding:
    def __init__(self, window_time=100):
        self.window = util.MovingWindow(window_time)

    def __call__(self, spikes, time):
        return self.window.process(spikes, time)
