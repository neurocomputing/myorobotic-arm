#!/bin/bash

# This script copies files from my homedirectory into the webserver directory.
# (use scp and SSH keys for a remote directory)
# A new directory is created every hour.



rigpower_time=90 # time needed to perform rig-power
vc_wait_start=30
vc_wait_stop=10
attempts_before_rigpower=5

# Create params_vX.csv based on search.csv
python runner.py

cp touse/params_v1.csv params_c0.csv

start_time=$(date +%s)
fail_counter=0

# This loop starts the brain and the controller
while true; do
	a=$(ps -edf | grep "[p]ython main" | wc -l)
	# Are the main, brain and control stopped/running?
	if [[ "$a" -ne 3 ]];
	then

		echo "Launching NRP Experiment through Virtual Coach"
		sed -i 's/stopped/running/g' sim_status.mem
		cle-virtual-coach myo_vc.py &
		sleep $vc_wait_start

		echo -e "\n\n\n Checking if Rig-Power Needed \n\n\n"
		if [ "$fail_counter" -gt "$attempts_before_rigpower" ];
		# Run Rig-Power if needed
		then
			echo -e "\n\n\n Rig-Power \n\n\n"
			rig-power 192.168.2.0; sleep 5
			fail_counter=0
			run_time=$((sim_duration+rigpower_time))
		else
			run_time=$((sim_duration))
			echo "fail_counter: $fail_counter"
			fail_counter=$((fail_counter+1))
		fi

		# Run Brain/Control
		start_time=$(date +%s)
		rm -rf runs/* ; rm -rf reports/*
		echo -e "\n\n\n Launching Brain/Controller \n\n\n"
		python main.py
		echo -e "\n\n\n Stopping Brain/Controller \n\n\n"
	fi
	time_elapsed=$(($(date +%s)-start_time))

	echo "time_elapsed: $time_elapsed"
	# has the experiment run for at enough time?
	if [ "$time_elapsed" -gt "$run_time" ];
	then
		echo "Stopping NRP Experiment through Virtual Coach"
		sed -i 's/running/stopped/g' sim_status.mem
		sleep $vc_wait_stop
		break
	fi
done

rm params_c0.csv
