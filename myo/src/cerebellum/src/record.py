import tensorboardX as tb

import time


class Recorder:
    """
    A class for recording Tensorboard values

    Usage:
    >>> layout = {...}
    >>> recorder = Recorder(layout)
    >>> rec_accuracy = recorder.scalar("accuracy")
    >>> rec_accuracy(73.1)
    >>> ...
    >>> rec_accuracy(78.4)

    One can also access the TensorboardX instance directly via `recorder.writer`
    """

    def __init__(self, layout, path=None):
        args = {"max_queue": 10000, "flush_secs": 5}
        if path:
            self.writer = tb.SummaryWriter(path, **args)
        else:
            self.writer = tb.SummaryWriter(**args)
        self.writer.add_custom_scalars(layout)

    def scalar(self, name):
        return ScalarWriter(self.writer, name)


class ScalarWriter:
    def __init__(self, writer, name):
        self.writer = writer
        self.name = name
        self.start = time.time()

    def __call__(self, value, timestamp=None):
        self.writer.add_scalar(self.name, value)
        if not timestamp:
            timestamp = time.time()
        timestamp = timestamp - self.start
        try:
            self.writer.add_scalar(self.name, value, timestamp)
        except Exception as e:
            pass
