from control import Controller
from physical_model import PhysicalSetup
import trajectory
import model
import rospy
import traceback
import queue
import time
import math


def _put_and_go(input_queue, value):
    try:
        input_queue.get(block=False)  # Remove old state
    except queue.Empty:
        pass  # Ignore an empty queue

    try:
        input_queue.put(value, False)  # Do not wait
    except queue.Full:
        pass  # Ignore a full queue


class LimbController:
    def __init__(self, limb):
        self.limb = limb
        self.desired_shoulder = 0.5  # Center
        self.motor_values = [0, 0]
        self.previous_angle = 0
        self.previous_velocity = 0
        self.previous_time = time.time()
        self.c = 0

    def move(self, desired_shoulder):
        self.desired_shoulder = LimbController.trajectory_normalise(desired_shoulder)
        _put_and_go(self.limb.desired, self.desired_shoulder)

        current_time = time.time()
        dt = current_time - self.previous_time
        alpha = 0.9
        shoulder_velocity_desired = (self.desired_shoulder - self.previous_angle) / dt
        shoulder_velocity_desired = alpha*shoulder_velocity_desired + (1-alpha)*self.previous_velocity
        shoulder_velocity_desired = min(1, max(0, shoulder_velocity_desired + 0.5))  # [0..1]
        self.previous_time = current_time
        self.previous_angle = self.desired_shoulder
        self.previous_velocity = shoulder_velocity_desired
        _put_and_go(self.limb.desired_v, shoulder_velocity_desired)

    def update(self, state):
        # Update actual (POPULATION CODING)
        angle = state.angle  # In [-pi/2;pi/2]
        norm_angle = angle/(2*0.88) + 0.5 # Gazebo Limits [0.8728*(-1,1)) ; 0.6471*(-1,1))]
        _put_and_go(self.limb.actual, norm_angle)

        # Velocity
        shoulder_velocity = min(1, max(0,(state.velocity / 2.5) + 0.5))
        _put_and_go(self.limb.actual_v, shoulder_velocity)

        # Update error (RATE CODING)
        error_left_pull, error_left_relax, error_right_pull, error_right_relax = self.limb_error(
            (norm_angle + 1) / 2, self.desired_shoulder
        )
        _put_and_go(self.limb.error_left_pull, error_left_pull)
        _put_and_go(self.limb.error_left_relax, error_left_relax)
        _put_and_go(self.limb.error_right_pull, error_right_pull)
        _put_and_go(self.limb.error_right_relax, error_right_relax)

        # Overall displacement should remain constant at 320
        min_disp = 10
        max_disp = 310

        try:
            if self.c % 10000 == 0:
                rospy.loginfo("Polling......")
            self.c += 1
            val_left = max(0.001,self.limb.out_left.get(False)) # Do not wait
            val_right = max(0.001,self.limb.out_right.get(False)) # Do not wait
            self.motor_values[0] = 10 + (val_left/(val_left+val_right))*300
            self.motor_values[1] = 10 + (val_right/(val_left+val_right))*300
        except queue.Empty:
            pass  # Ignose empty queue

        return self.motor_values

    def limb_error(self, current_angle, desired_angle, scaling=20):
        left_pull = self.motor_values[0]
        right_pull = self.motor_values[1]

        e = (desired_angle-current_angle) # [-1;1]

        error_right_pull = max(0, -e)
        error_left_pull = max(0, e)
        error_right_relax = error_left_pull
        error_left_relax = error_right_pull

        return (error_left_pull * scaling, error_left_relax * scaling,
                error_right_pull * scaling, error_right_relax * scaling )

    @staticmethod
    def trajectory_normalise(angle):
        # Max angle is +/- 55
        return angle / 2 + 0.5


class BrainController(model.Model):
    def __init__(
        self, upper_arm, frequency, dt, mode="simulation",
    ):
        """
        Creates a motor controller counterpart to a brain

        Arguments:
          input_queue -- The queue to send state signals _into_ the brain
          error_queue -- The queue to send error signals _into_ the brain
          output_queue -- The queue to retrieve motor signals _from_ the brain
          mode -- Simulation or physical
        """
        super(BrainController, self).__init__(mode, frequency=frequency, dt=dt)
        self.controller_shoulder = LimbController(upper_arm)
        self.motor_values = [0, 0, 310, 10]

    def update(self, msg):
        # Skip if the controller has not been initialised
        if not hasattr(self, "controller_shoulder"):
            return

        try:
            (shoulder_left, shoulder_right) = self.controller_shoulder.update(
                msg.shoulder
            )
            self.motor_values[0] = shoulder_left
            self.motor_values[1] = shoulder_right
            self.controller.send("motor", self.motor_values)
        except Exception as e:
            rospy.logerr("Error when updating model: " + str(type(e)))
            rospy.logerr(traceback.format_exc())

    def phase_motor_babbling(self, trajectory):
        try:
            # Clear the robot
            self.controller.send("motor", self.motor_values)
            while True:
                # Calculate desired (POPULATION CODING)
                shoulder_angle, _ = next(trajectory)
                self.controller_shoulder.move(shoulder_angle)
                # Sleep until next update
                self.controller.rate.sleep()
        except Exception as e:
            rospy.logerr("Error when executing motor babbling: " + str(type(e)))
            rospy.logerr(traceback.format_exc())

        rospy.loginfo("Done with motor babbling")

    def phase_throwing(self, trajectory):
        # Ignore for now
        # raise NotImplementedError
        pass
