#!/usr/bin/env python
import matplotlib.pyplot as plt
import sys
import time
import math
import numpy as np
from numpy import genfromtxt

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

import pdb
import csv
import glob
import os


#  python3 plotter.py summary/last/
if __name__ == "__main__":


    folder_path = sys.argv[1]
    try_list = next(os.walk(folder_path))[1]
    for i in try_list:
        list_actual = []
        list_desired = []
        try:

            path = folder_path + i + '/'

            title = "PARAMETERS\n"
            sim_duration = float("inf")
            filename = path+"params_c0.csv"
            title = folder_path + ',' + i + ','
            with open(filename) as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    title += row['Value'] + ","
                    if "sim_duration" in row['Parameter']:
                        sim_duration = float(row['Value'])
            # pdb.set_trace()

            title += title + '\n'
            f = open('foundpars.csv','a')
            f.write(title)
            f.close()



            t = 0.05
        except:
            t = 0.1
            pass
        time.sleep(t)
