from multiprocessing import Queue


class Limb(object):
    def __init__(self, name):
        self.name = name
        self.actual = Queue(1)
        self.desired = Queue(1)
        self.error_left_pull = Queue(1) # How much you need to pull in the left muscle
        self.error_left_relax = Queue(1)
        self.error_right_pull = Queue(1)
        self.error_right_relax = Queue(1)
        self.out_left = Queue(1)
        self.out_right = Queue(1)
        self.actual_v = Queue(1)
        self.desired_v = Queue(1)

    def close(self):
        self.actual.close()
        self.desired.close()
        self.error_left_pull.close()
        self.error_left_relax.close()
        self.error_right_pull.close()
        self.error_right_relax.close()
        self.out_left.close()
        self.out_right.close()
        self.actual_v.close()
        self.desired_v.close()
