#!/bin/bash

# This script copies files from my homedirectory into the webserver directory.
# (use scp and SSH keys for a remote directory)
# A new directory is created every hour.

rigpower_time=90 # time needed to perform rig-power
vc_wait_start=30
vc_wait_stop=10
attempts_before_rigpower=5

# Create params_vX.csv based on search.csv
python runner.py

# Run simulation for each params_vX.csv found in dir 'touse/'
for file in touse/*
do
    echo -e "\nStart Simulation for ${file#touse/}\n"
	cat $file
    var=$(grep "sim_duration" $file)
    sim_duration="${var#sim_duration,}"
	sim_duration="${sim_duration%.*}"
	cp $file params_c0.csv

	# Remove trash folder if necessary
	rm results/* 2> /dev/null
	rm -rf runs/ 2> /dev/null
	rm -rf summary/last 2> /dev/null
	rm -rf application_generated_data_files/

	start_time=$(date +%s)
	sim_name="Try-$(date +%F-%Hh%M)"
	mkdir "$sim_name"

	# This loop starts the brain and the controller
	while true; do
		a=$(ps -edf | grep "[p]ython main" | wc -l)
		# Are the main, brain and control stopped/running?
		if [[ "$a" -ne 3 ]]; then

			if [ ! "$1" = "novc" ]; then
				echo "Launching NRP Experiment through Virtual Coach"
					sed -i 's/running/stopped/g' sim_status.mem
				sleep $vc_wait_stop
				pid_myo_vc=$(ps -ef | grep "myo_vc.py" | tr -s ' ' | cut -d ' ' -f2 | head -1)
				kill -9 "$pid_myo_vc" &>/dev/null
				sed -i 's/stopped/running/g' sim_status.mem
				cle-virtual-coach myo_vc.py &
				sleep $vc_wait_start
			fi

			# Restart the rig
			echo -e "\n\n\n Checking if Rig-Power Needed \n\n\n"
			rig-power 192.168.2.0; sleep 5
			run_time=$((sim_duration+rigpower_time))

			# Run Brain/Control
			start_time=$(date +%s)
			rm -rf reports/*
			echo -e "\n\n\n Launching Brain/Controller \n\n\n"
			python main.py 1>> "$sim_name/test.log"  2>> "$sim_name/error.log" 
			echo -e "\n\n\n Stopping Brain/Controller \n\n\n"
		fi
		time_elapsed=$(($(date +%s)-start_time))

		echo "time_elapsed: $time_elapsed"
		# has the experiment run for at enough time?
		if [ "$time_elapsed" -gt "$run_time" ];
		then
			echo "Stopping NRP Experiment through Virtual Coach"
			sed -i 's/running/stopped/g' sim_status.mem
			sleep $vc_wait_stop
			break
		fi
	done

    sed -i 's/running/stopped/g' sim_status.mem

	# Saving Everything in One Folder
	cp -r runs/* "$sim_name" ;
	mv reports/* "$sim_name"
	mv results/* "$sim_name"
	mv params_c0.csv "$sim_name"
	cp -r "$sim_name" summary/last
	mv "$sim_name" summary/

	# Move params_vX.csv from dir 'touse/' to dir 'used/'
    mv $file used/
    echo "End Simulation for ${file#touse/}"
done

echo -e "\n\n\n Simulation properly stopped \n\n\n"
