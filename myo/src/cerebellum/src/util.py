# Thanks to https://stackoverflow.com/questions/39166941/real-time-moving-averages-in-python
class MovingWindow:
    def __init__(self, window_size):
        self.window_size = window_size
        self.values = []
        self.last_time = 0
        self.sum = 0

    def process(self, value, time):
        # Append zeros
        td = int(time - self.last_time) - 1
        self.last_time = time

        # Process actual value
        if td >= self.window_size:
            self.values = [value]
            return value / self.window_size
        else:
            self.values.append(value)
            self.values = self.values[-(self.window_size - td):]
            return float(sum(self.values)) / self.window_size


import threading

# Thanks to https://stackoverflow.com/questions/47912701/python-how-can-i-implement-a-stoppable-thread
class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()
