#!/usr/bin/env python
import matplotlib.pyplot as plt
import sys
import time
import math
import numpy as np
from numpy import genfromtxt

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

import csv
import glob
import os

path = sys.argv[1]

try_list = next(os.walk(path))[1]
print(try_list)
for i in try_list:
    list_actual = []
    list_desired = []
    try:

        print(i)

        # Get data from mossy fibers (actual)
        fn_actual = path+ i + '/Upper_mf_a_actual.csv'
        with open(fn_actual) as csvfile:
            list_actual = list(csv.reader(csvfile))
        array_actual = np.asarray(list_actual, dtype=np.float64)

        # Get data from mossy fibers (desired)
        fn_desired = path+ i + '/Upper_mf_a_desired.csv'
        with open(fn_desired) as csvfile:
            list_desired = list(csv.reader(csvfile))
        array_desired = np.asarray(list_desired, dtype=np.float64)

        # Interpolation
        minX = min(min(array_actual[:,0]), min(array_desired[:,0]))
        maxX = max(max(array_actual[:,0]), max(array_desired[:,0]))
        lenX = 2*max(len(array_actual[:,0]), len(array_desired[:,0]))
        new_x = np.linspace(minX, maxX, lenX, dtype=np.float64)
        new_y_actual = np.interp(new_x, array_actual[:,0], array_actual[:,1])
        new_y_desired = np.interp(new_x, array_desired[:,0], array_desired[:,1])

        error = abs((new_y_actual-new_y_desired))
        # error = 100*abs((new_y_actual-new_y_desired)/new_y_desired)

        plt.figure(figsize=(24, 12))
        # TO DO:  Use subplots: print error and actual signals

        plt.plot(new_x-minX, error)
        # plt.axis([(maxX-minX)/4+minX, maxX, 0, 100])
        plt.title(i)

        plt.show()

        t = 0.5
    except:
        t = 1
        pass
    print('tt')

    # old_x = np.linspace(0, 2*np.pi, 10)
    # old_y = np.sin(old_x)
    # new_x =np.linspace(0, 2*np.pi, 100)
    # new_y = np.interp(new_x, old_x, old_y)
    # plt.plot(old_x, old_y, 'o')
    # plt.plot(new_x, new_y, '-x')
    # plt.show()
    time.sleep(t)
