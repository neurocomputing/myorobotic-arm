# Cerebellum model

This package contains 

1) Control logic for asynchronous interaction with the Neurorobotics Platform (NRP) myorobotic arm experiment and
2) A cerebellum-based model for myorobotic motor control running on a SpiNNaker board

The model is implemented following this paper by [Abadía et al.](https://pubmed.ncbi.nlm.nih.gov/31647453/) and looks roughly like the following:

```
                                 CF_l        CF_r
                                   |          |
                                   |          |
                                   |          |
                          STDP     |          |
    GC_a --------------+-----------|----/\    |
      |                |           |   /  \   |
      |   GC_v---------+           |  /-  -\  |
      |    |                       |-        -|
      |    |                      PC_l      PC_r
      |    |                       |          |
      |----|---------+-------------|----/\    |
      |    |         |             |   /  \   |
     MF_a  |         |             |  /-  -\  |
           |         |             |-        -|
          MF_v-------+           DCN_l      DCN_r

```

CF = Climbing fibers, GC = Granule cells, PC = Purkinje cells, MF = Mossy fibers, DCN = Neep cerebellar nuclei.

## Prerequisites

* A SpiNNaker board compatible with the [SpyNNaker8 framework](https://github.com/SpiNNakerManchester/sPyNNaker8), with at least ~50 cores.
* An NRP setup following the [myorobotic arm installation instructions](https://gitlab.com/neurocomputing/myorobotic-arm).

If you wish to run the physical experiment, you need access to the myorobotic arm itself.

## Installation

1. Ensure connectivity to the SpiNNaker board and correct configuration of a `.spynnaker.cfg` config file. See for instance [setup instructions for the SpiNNaker board](https://gitlab.com/neurocomputing/myorobotic-arm/-/blob/master/spinnaker_setup.md).
2. Install cerebellum-specific packages by entering the `myo` directory and running the Makefile with: `make cerebellum`

## Configuration of model parameters

The simulations will read parameters from the [params.csv](https://gitlab.com/neurocomputing/myorobotic-arm/-/blob/master/myo/src/cerebellum/src/params.csv) file.

Here is a brief legend:

| Param name | Meaning |
| --- | --- |
| sim_duration | Duration of the simulation in seconds
| w_* | Weight parameter
| inout_pop_size | The **total** number of neurons in MF, CF, and DCN populations (I/O populations)
| mf_share | The percentage share of MF neurons out of all I/O neurons
| gc_pc_ratio | Ratio between number of GC and number of PC neurons
| window_t | Size of moving average window for DCN spike reading


## Running the cerebellum model

Simply enter the `myo/src/cerebellum/src` directory and execute `. ./launch_once.sh` (note the preceeding `.`!).

Note that the NRP are currently controlled using the Virtual Coach. This automates the starting, running, and stopping of the NRP, and prevents memory leaks over time. The CLE can be disabled in the [launch_once.sh](https://gitlab.com/neurocomputing/myorobotic-arm/-/blob/master/myo/src/cerebellum/src/launch_once.sh#L31) script line 31. Note, however, that that requires that you clone and run the experiment manually *before* the launch script has been started.
